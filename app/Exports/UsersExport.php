<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Excel;

class UsersExport implements FromCollection, WithMapping, WithColumnFormatting, WithHeadings, ShouldAutoSize

{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::all();
    }

    /**
     * @var User $user
     * @return array
     */
    public function map($user): array
    {
      //dd($user->roles[0]->label);
      $status_text=array('Inactif','Actif');

      return [
        $user->id,
        $user->fname,
        $user->phone,
        $user->email,
        $user->gender,
        $user->father_name,
        $user->dob,
        $user->is_verified ? 'Yes' : 'No',
      ];
        //'=A2+1',
    }
    public function headings(): array
    {
        return [
            'ID',
            'Name',
            'Phone',
            'Email',
            'Gender',
            'Father Name',
            'Date of Birth',
            'Verified',
        ];
    }
    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_TEXT,
        ];
    }
}
