<?php
namespace App\Helpers;

class Helper{
    
    public static function getIconForFile($extension) {
        switch(true) {
            case ($extension == 'png' OR $extension == 'jpeg' OR $extension == 'jpg'):
                $icon = 'fa fa-file-image-o';
                break;
            case $extension == 'pdf':
                $icon = 'fa fa-file-pdf-o';
                break;
            case $extension == 'docs':
                $icon = 'fa fa-file-o';
                break;
            default:
                $icon = 'fa fa-file-o';
                break;
        }
        return $icon;
    }

    public static function date_sort($a, $b) {
        return strtotime($a) - strtotime($b);
    }
    
}// End of class
