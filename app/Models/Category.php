<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    public function Users() {
        return $this->hasMany(User::class);
    }
    public function business(){
        return $this->hasOne(business::class);
    }
    public function subcategory(){
        return $this->hasMany(Subcategory::class);
    }
   
}
