<?php

namespace App\Models;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    use HasFactory;
    public function Users(){
        return $this->hasMany(User::class);
    }
    public function category(){
        return $this->belongsTo('Category::class');
    }
    public function business(){
        return $this->hasOne('business::class');
    }
}
