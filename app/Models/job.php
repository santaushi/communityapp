<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class job extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'field',
        'experience',
        'ctc',
        'location'
    ];
    public function Users(){
        return $this->belongsTO(User::class);
    }
}
