<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\CompanyProfile;
use App\Models\NativeProfile;
use App\Models\Addmember;
use App\Models\business;
use App\Uuids as Uuids;
class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use Uuids;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $primarykey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        'business_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAge(){
        $this->birthdate->diff(Carbon::now())
             ->format('%y years, %m months and %d days');
    }
    
    public  function Roles() {
        return $this->belongsTo(Role::class);
    }

    public function CompanyProfile()
    {
        return $this->belongsTo(CompanyProfile::class, 'id', 'user_id')->withDefault([
            "id"=>'',
            "user_id"=>"",
            "company_name"=>'',
            "company_address"=>"",
            "office_area"=>'',
            "company_city"=>'',
            "company_state"=>'',
            "company_country"=>'',
            "office_phone"=>'',
            "company_email"=>'',
            "company_website"=>'',
            "company_image"=>'',
            "category_id"=>"",
            "subcategory_id"=>"",
            "created_at"=>"",
            "updated_at"=>"",
            "category" => [
                "id"=>'',
                "categories_name"=>"",
                "categories_image"=>"",
                "created_at"=>"",
                "updated_at"=>""
            ]
        ]);
    }

    public function ResidenceProfile() {
        return $this->belongsTo(ResidenceProfile::class, 'id', 'user_id')->withDefault([
            "id"=>'',
            "user_id"=>"",
            "address"=>"",
            "area"=>'',
            "city"=>"",
            "pincode"=>"",
            "primary_phone"=>'',
            "secondary_phone"=>'',
            "country"=>"",
            "created_at"=>"",
            "updated_at"=>""
        ]);
    }

    public function Categories() {
        return $this->belongsTo(Category::class, 'category_id', 'id')->withDefault([
            "id"=>'',
            "categories_name"=>"",
            "categories_image"=>"",
            "created_at"=>"",
            "updated_at"=>""
        ]);
    }

    public function Native() {
        return $this->belongsTo(NativeProfile::class, 'id', 'user_id')->withDefault([
            "id"=>'',
            "user_id"=>"",
            "native_address"=>"",
            "native_pincode"=>'',
            "committe_member"=>'',
            "committe_role"=>'',
            "committe_order"=>'',
            "created_at"=>"",
            "updated_at"=>""
        ]);
    }

    public function Addmembers() {
        return $this->hasMany(Addmember::class, 'user_id');
    }
    public function SubCategory(){
        return $this->belongsTo(Subcategory::class);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function business(){
        return $this->belongsTo(business::class);
    }
    public function Jobs(){
        return $this->hasOne(job::class,'user_id');
    }
}
