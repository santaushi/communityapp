<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Mail; 
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function forgotPassword(){
        return view('auth.passwords.email');
    }

    public function sendResetPassWordLink(Request $request){
        $request->validate(['email' => 'required|email']);

        if (User::where('email',  $request->email)->exists())
        {
            $token = Str::random(64);

            DB::table('password_resets')->insert(
                ['email' => $request->email, 'token' => $token, 'created_at' => Carbon::now()]
            );

            Mail::send('auth.custom_verify.verify', ['token' => $token], function($message) use($request){
                $message->to($request->email);
                $message->subject('Reset Password Notification');
            });

            return back()->with('message', 'We have e-mailed your password reset link!');
        } else {
            return back()->with('error', 'We have not found this email in our record!');
        }


        
  
    }
}
