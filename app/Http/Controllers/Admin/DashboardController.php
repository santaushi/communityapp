<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Event;
use Carbon\Carbon;
use DB;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    
    public function index() {
        $user = User::get();
        $events = Event::get();
        $usercount = count($user);
        $TotalEvent = count($events);


        
        if(Auth::check() && Auth::user()->role_id == 1) {
            return view('admin.pages.index',compact('usercount','TotalEvent'));
        }
        if(Auth::check() && Auth::user()->role_id == 2) {
            return 'this is user';
        }
    }

    public function getUsersForChart(){
        $usersChart = User::orderBy('created_at')->get()->groupBy(function($date) {
            //return Carbon::parse($date->created_at)->format('Y'); // grouping by years
            return Carbon::parse($date->created_at)->format('Y-m-d'); // grouping by months
        });
        return response()->json($usersChart, 200);
    }
}
