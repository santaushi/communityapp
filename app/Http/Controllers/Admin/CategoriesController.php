<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Validator;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\business;
class CategoriesController extends Controller
{
    public function index() {
        $categories =  Category::get();
        return view('admin.pages.categories', compact('categories'));
    }

    public function store(Request $request) {
       
        $request->validate([
            'categories_image' => ['required'],
            'categories_name' => ['required','unique:categories'],

        ]);

        $categories =  new Category();

        if ($request->hasFile('categories_image')) {
            $file = $request->file('categories_image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move('admin/categories/', $filename);
            $categories->categories_image = 'admin/categories/'.$filename;
        }
        $categories->categories_name = $request->categories_name;
        $categories->save();
        return redirect()->back()->with(['status' => 'Categories Added successFully saved']);
        // return response()->json(['message' =>  'data saved successfully']);
    }

    public function edit($id) {
        $categories_edit =  Category::find($id);
        if($categories_edit) {
            $categories =  Category::get();
            return view('admin.pages.categories', compact('categories_edit', 'categories'));
        }
    }

    public function update(Request $request, $id) {

              
        if ($request->hasFile('categories_image')) {
            $category = Category::find($id);
            $categoryImage= $category->categories_image;
            unlink($categoryImage); 
            
            $file = $request->file('categories_image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move('admin/categories/', $filename);
            Category::where('id', $id)->update([
                'categories_image' => 'admin/categories/'.$filename,
            ]);
        }

        Category::where('id', $id)->update([
            'categories_name' => $request['categories_name']
        ]);
        return redirect()->route('categories.index')->with(['status' => 'Categories Updated SuccessFully Update!']);
    }
    public function destroy($id){
            $category = Category::find($id);
            $category->delete();
            return redirect()->back()->with('remove','Category has been deleted');
    }

    public function remove_categories($id) {

        $remove_categories = Category::find($id);
        $remove_categories->delete();
        return response()->json(['remove_categories' =>  $remove_categories]);  
    }
    public function show($id)
    {
        //
    }
    public function findsubcategory($id){
        $subcategory = Subcategory::where('category_id',$id)->get();
        //dd($subcategory);
        return response()->json($subcategory);
       
        
    }
    public function findOldsubCategory($id){
        $business = business::where('id',$id)->first();
        $subcategoryId = $business->subcategory_id;
        $subcategory = Subcategory::where('id',$subcategoryId)->first();
        //dd($subcategory);
        return response()->json($subcategory);
    }
    public function findCategoryforSubcategory($id){
        $subcategory = Subcategory::where('id',$id)->first();
        $categoryId = $subcategory->category_id;
        $category = Category:: where('id',$categoryId)->get();
        return response()->json($category);
    }
    public function findBusinessforCategory($id){
        $business = business::where('category_id',$id)->get();
        return response()->json($business);
    }
        
    
}
