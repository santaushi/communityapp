<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CompanyProfile;
use App\Models\NativeProfile;
use App\Models\ResidenceProfile;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\business;
use App\Models\job;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Auth;
class UserController extends Controller
{
    public function index() {
        $users = user::with('CompanyProfile', 'ResidenceProfile','Addmembers')->where('role_id', 2)->get();
        //dd($users);
        return view('admin.pages.users',compact('users'));   
    }
    public function create_user() {
        $categories =  Category::get();
        $subcategories =  Subcategory::get();
        $businesses = business::get();
        return view('admin.pages.user_create', compact('categories', 'subcategories','businesses'));
    }
    public function store(Request $request) {
       //dd($request->all());
        $request->validate([
            'fname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'gender' => ['required'],
            'phone' =>  ['required', 'unique:users'],
            
            'dob' => ['required'],
            'father_name' => ['required', 'string', 'max:255'],
            
            'area' => ['required'],
            'address' => ['required'],
            
            'city' => ['required'],
            'pincode' => ['required'],
           
            'primary_phone' => ['required'],
            
            // 'country' => ['required'],
            //dd('1'),
        ]);
        //dd('1');
        $user =  new User();
        if ($request->hasFile('image')) {
            $path = 'users/images';
            $file = $request->file('image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path($path), $filename);
            $user->image =$path. '/' .$filename;
            
        }
        //dd('1');
        if($request->job=="on"){
            $user->job = 1;
        }
        if($request->matrimonial=="on"){
            $user->matrimonial = 1;
        } 
        $user->fname = $request->fname;
        $user->mname = $request->mname;
        $user->lname = $request->lname;
        $user->email = $request->email;
       

        $user->phone =  $request->phone;
        $password_generate =  str::random(8);
        $user->password = Hash::make($password_generate);
        $user->secondary_email =  $request->secondary_email;
        $user->gender =  $request->gender;
        $user->father_name =  $request->father_name;
        $user->fathernumber = $request->fathernumber;
        $user->dob =  $request->dob;
        
        // $user->date_of_anniversary = $request->date_of_anniversary;
        $user->role_id =  2;
       
        $user->business_id = $request->business_id;
        $user->save();

        if($request->job=="on"){
            $job = new job();
            $job->user_id = $user->id;
            $job->field = $request->jobfield;
            $job->location = $request->joblocation;
            $job->experience = $request->jobexperience;
            $job->ctc = $request->jobctc;
            $job->save();
        }
      
        $residenceProfile =  new ResidenceProfile();
        $residenceProfile->user_id = $user->id;
        $residenceProfile->address =   $request->address;
        $residenceProfile->area =   $request->area;
        $residenceProfile->city = $request->city;
        $residenceProfile->pincode = $request->pincode;
        $residenceProfile->primary_phone = $request->primary_phone;
        // $residenceProfile->secondary_phone = $request->secondary_phone;
        $residenceProfile->country = $request->country;
        //dd('1');
        $residenceProfile->save();
        
        $CompanyProfile =  new CompanyProfile();
        $CompanyProfile->user_id =  $user->id;
        $CompanyProfile->company_name = $request->company_name;
        $CompanyProfile->company_address = $request->company_address;
        $CompanyProfile->office_area = $request->office_area;
        $CompanyProfile->company_city = $request->company_city;
        $CompanyProfile->company_state = $request->company_state;
        $CompanyProfile->company_country = $request->company_country;
        $CompanyProfile->office_phone = $request->office_phone;
        $CompanyProfile->company_email = $request->company_email;
        $CompanyProfile->company_website = $request->company_website;
        $CompanyProfile->category_id = $request->category_id;
        $CompanyProfile->subcategory_id =  $request->subcategory_id;
        
        User::where(['id' => $user->id])->update([
            'category_id' => $request['category_id'],
            'subcategory_id' => $request['subcategory_id'],

        ]);

        if ($request->hasFile('company_image')) {
            $file = $request->file('company_image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move('users/company-image/', $filename);
            $CompanyProfile->company_image = $filename;
        }
        
        $CompanyProfile->save();


        $nativeProfile =  new NativeProfile();
        $nativeProfile->user_id =  $user->id;
        $nativeProfile->native_address  = $request->native_address;
        $nativeProfile->native_city  = $request->native_city;
        $nativeProfile->native_pincode =  $request->native_pincode;
        $nativeProfile->committe_member =  $request->committe_member;
        $nativeProfile->committe_role =  $request->committe_role;
        $nativeProfile->committe_order =  $request->committe_order;
        //dd('1');
        $nativeProfile->save();
        return redirect()->back()->with('status', 'user data successfully Added');
    }

    public function edit($id) {
        
        $useredit = User::find($id);
        if($useredit) {
            $users = user::with('CompanyProfile', 'ResidenceProfile', 'Native', 'Categories','Jobs')->get();
            $categories =  Category::get();
            $subcategories =  Subcategory::get();
            $businesses = business::get();
            //dd($useredit);
            return view('admin.pages.users-edit', compact('useredit', 'users', 'categories', 'subcategories','businesses')); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $useredit = User::find($id);
        // dd($useredit);
        if($useredit) {
            $users = user::with('CompanyProfile', 'ResidenceProfile', 'Native', 'Categories','Jobs')->get();
            $categories =  Category::get();
            $subcategories =  Subcategory::get();
            return view('admin.pages.user_create', compact('useredit', 'users', 'categories', 'subcategories')); 
        }else{
            return redirect()->back()->withErrors(['error'=>'No user found...'])->withInput();
        }
        
    }


    public function update(Request $request , $id) {
      //dd($request->all());
        $request->validate([
            // 'fname' => ['required', 'string', 'max:255'],
           
            // 'gender' => ['required'],
            // 'phone' =>  ['required',],
            'dob' => ['required'],
            
            // 'father_name' => ['required', 'string', 'max:255'],
            // 'area' => ['required'],
            // 'address' => ['required'],
            // 'city' => ['required'],
            // 'pincode' => ['required'],
            // 'primary_phone' => ['required'],
            
            // 'country' => ['required'],
            
        ]);
        //dd('1');
        if ($request->hasFile('image')) {
            $path = 'users/images';
            $file = $request->file('image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move('users/images/', $filename);
            User::where('id', $id)->update([
                'image' => $path.'/'.$filename,
            ]);
        }
        if($request->job=="on"){
            User::where('id',$id)->update([
                'job' => 1,
            ]);
        }else{
            User::where('id',$id)->update([
                'job'=> 0,
            ]);
        }
        if($request->matrimonial=="on"){
            User::where('id',$id)->update([
                'matrimonial' => 1,
            ]);
        }else{
            User::where('id',$id)->update([
                'matrimonial'=> 0,
            ]);
        }
        User::where('id', $id)->update([
            'fname' => $request['fname'],
            'mname' => $request['mname'],
            'lname' => $request['lname'],
            'education' => $request['education'],
            'business_id' => $request['business_id'],
            // 'phone' => $request['phone'],
            // 'secondary_email' => $request['secondary_email'],
            'gender' => $request['gender'],
            'dob' => $request['dob'],
            'father_name' => $request['father_name'],
            'fathernumber' => $request['fathernumber'],
            // 'date_of_anniversary' => $request['date_of_anniversary'],
        ]);

        if ($request->email) {
            $request->validate([
                'email' => ['string', 'email', 'max:255'],
            ]);
          
            User::where('id', $id)->update([
                'email' => $request['email'],
                'email_verified_at' => NULL,
            ]);
        }

        ResidenceProfile::where('user_id', $id)->update([
                'address' => $request['address'],
                'area' => $request['area'],
                'city' => $request['city'],
                'pincode' => $request['pincode'],
                'primary_phone' => $request['primary_phone'],
                // 'secondary_phone' => $request['secondary_phone'],
                'country' => $request['country'],
        ]);
        $job = job::where('user_id',$id)->get();
        if(count($job)!==0){
            job::where('user_id',$id)->update([
            
                'field' => $request['jobfield'],
                'location' => $request['joblocation'],
                'experience' => $request['jobexperience'],
                'ctc' => $request['jobctc'],
            ]);
        }else{
            $jobs = new job();
            $jobs->user_id = $id;
            $jobs->field = $request->jobfield;
            $jobs->location = $request->joblocation;
            $jobs->experience = $request->jobexperience;
            $jobs->ctc = $request->jobctc;
            $jobs->save();
        }
       
        CompanyProfile::where('user_id', $id)->update([
            'company_name' =>  $request['company_name'],
            'company_address' =>  $request['company_address'],
            'office_area' =>  $request['office_area'],
            'company_city' =>  $request['company_city'],
            'company_state' =>  $request['company_state'],
            'company_country' =>  $request['company_country'],
            'office_phone' =>  $request['office_phone'],
            'company_email' =>  $request['company_email'],
            'company_website' =>  $request['company_website'],
            'category_id' => $request['category_id'],
            'subcategory_id' => $request['subcategory_id'],
        ]);
        User::where('id',$id)->update([
            'category_id' => $request['category_id'],
            'subcategory_id' => $request['subcategory_id'],

        ]);

        if ($request->hasFile('company_image')) {
            $request->validate([
                'company_image' => 'image|mimes:jpg,png',
            ]);
            $pathForcompanyImage = 'users/company-image';
            $file = $request->file('company_image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move('users/company-image/', $filename);
            CompanyProfile::where('user_id', $id)->update([
                'company_image' =>  $pathForcompanyImage.'/'.$filename,
            ]);
        }

        NativeProfile::where('user_id', $id)->update([
            'native_address' => $request['native_address'],
            'native_city' => $request['native_city'],
            'native_pincode' => $request['native_pincode'],
            'committe_member' => $request['committe_member'],
            // 'committe_role' => $request['committe_role'],
            // 'committe_order' => $request['committe_order'],
        ]);

        return redirect()->route('users.index')->with('status', 'Profile has been updated successfully!');
    }
    public function remove_user($id) {
        $user_record =  User::find($id);
        $user_record->delete();
        return redirect()->back()->with('remove','Deleted Successfully!');
        //return response()->json(['user_record' => $user_record]);
    }

    public function editProfile($id){
        $user = User::with('CompanyProfile', 'ResidenceProfile')->find($id);
        //dd($user);
        if(!is_null($user)){
            $categories =  Category::get();
            $subcategories =  Subcategory::get();
            return view('admin.pages.edit-profile',compact('user','categories','subcategories'));
        }else{
            return redirect()->back()->withErrors(['msg' => 'Can not find user.']);
        }
        
    }
    public function admin(){
        $categories =  Category::get();
        $subcategories =  Subcategory::get();
        $businesses = business::get();
        return view('admin.pages.adminedit-profile',compact('categories','subcategories','businesses'));
    }

    public function updateProfileofAdmin(Request $request){
        $id = Auth::user()->id;
        // $user = User::find($id);
        if ($request->hasFile('image')) {
            $path = 'users/images';
            $file = $request->file('image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move('users/images/', $filename);
            User::where('id', $id)->update([
                'image' => $path.'/'.$filename,
            ]);
        }
        if($request->job=="on"){
            User::where('id',$id)->update([
                'job' => 1,
            ]);
        }else{
            User::where('id',$id)->update([
                'job'=> 0,
            ]);
        }
        if($request->matrimonial=="on"){
            User::where('id',$id)->update([
                'matrimonial' => 1,
            ]);
        }else{
            User::where('id',$id)->update([
                'matrimonial'=> 0,
            ]);
        }
        User::where('id', $id)->update([
            'fname' => $request['fname'],
            'mname' => $request['mname'],
            'lname' => $request['lname'],
            'education' => $request['education'],
            'business_id' => $request['business_id'],
            'phone' => $request['phone'],
            // 'secondary_email' => $request['secondary_email'],
            'gender' => $request['gender'],
            'dob' => $request['dob'],
            'father_name' => $request['father_name'],
            'fathernumber' => $request['fathernumber'],
            // 'date_of_anniversary' => $request['date_of_anniversary'],
        ]);

        // if ($request->email) {
        //     $request->validate([
        //         'email' => ['string', 'email', 'max:255'],
        //     ]);
          
        //     User::where('id', $id)->update([
        //         'email' => $request['email'],
        //         'email_verified_at' => NULL,
        //     ]);
        // }

        ResidenceProfile::where('user_id', $id)->update([
                'address' => $request['address'],
                'area' => $request['area'],
                'city' => $request['city'],
                'pincode' => $request['pincode'],
                'primary_phone' => $request['primary_phone'],
                // 'secondary_phone' => $request['secondary_phone'],
                'country' => $request['country'],
        ]);

        CompanyProfile::where('user_id', $id)->update([
            'company_name' =>  $request['company_name'],
            'company_address' =>  $request['company_address'],
            'office_area' =>  $request['office_area'],
            'company_city' =>  $request['company_city'],
            'company_state' =>  $request['company_state'],
            'company_country' =>  $request['company_country'],
            'office_phone' =>  $request['office_phone'],
            'company_email' =>  $request['company_email'],
            'company_website' =>  $request['company_website'],
            'category_id' => $request['category_id'],
            'subcategory_id' => $request['subcategory_id'],
        ]);
        if ($request->hasFile('company_image')) {
            $request->validate([
                'company_image' => 'image|mimes:jpg,png',
            ]);
            $pathForcompanyImage = 'users/company-image';
            $file = $request->file('company_image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move('users/company-image/', $filename);
            CompanyProfile::where('user_id', $id)->update([
                'company_image' =>  $pathForcompanyImage.'/'.$filename,
            ]);
        }

        NativeProfile::where('user_id', $id)->update([
            'native_address' => $request['native_address'],
            'native_city' => $request['native_city'],
            'native_pincode' => $request['native_pincode'],
            'committe_member' => $request['committe_member'],
            // 'committe_role' => $request['committe_role'],
            // 'committe_order' => $request['committe_order'],
        ]);
        return redirect()->back()->with('status', 'Profile has been updated successfully!');
    }

 
}
