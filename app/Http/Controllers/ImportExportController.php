<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Exports\UsersExportCSV;
// use Maatwebsite\Excel\Facades\Excel;
use App\Models\User;

class ImportExportController extends Controller
{
    /**
     * Export all users as excel.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function exportUsersExcel() 
    {
        return Excel::download(new UsersExport, 'Users_export-'.date('Y-m-d').'.xlsx');
    }

    /**
     * Export all users as CSV.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function exportUsersCSV() 
    {
        $fileName = 'Users_export-'.date('Y-m-d').'.csv';
        $users = User::all();

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );


        $columns = array('ID',
        'Name',
        'Phone',
        'Email',
        'Gender',
        'Father Name',
        'Date of Birth',
        'Verified',);

        $callback = function() use($users, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($users as $user) {
                $row['ID']  = $user->id;
                $row['Name']  = $user->fname;
                $row['Phone']    = $user->phone;
                $row['Email']     = $user->email;
                $row['Gender']     = $user->gender;
                $row['Father Name']  = $user->father_name;
                $row['Date of Birth']    = $user->dob;
                $row['Verified']    = $user->is_verified ? 'Yes' : 'No';

                fputcsv($file, array($row['ID'], $row['Name'], $row['Phone'], $row['Email'], $row['Gender'], $row['Father Name'], $row['Date of Birth'], $row['Verified']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);

    }

    /**
     * Export all users as PDF.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function exportUsersPDF() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
    
}
