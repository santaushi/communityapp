<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\Category;
use App\Models\business;
class BusinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $BusinessCategory = business::with('category','subcategory')->get();  
        //dd( $BusinessCategory) ;
        return view('admin.business.index',compact('BusinessCategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::get();
        return view('admin.business.create',compact('categories'));
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       //dd($request->all());
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
            'category'=>'required',
            'subcategory'=>'required',
        ]);
        $path = 'users/businesses';
        $business = new business();
        $business->title = $request->title;
        $business->description = $request->description;
        $business->category_id = $request->category;
        $business->subcategory_id = $request->subcategory;
        $imageName = time().'.'.$request->image->extension();  
        $request->image->move(public_path($path), $imageName);

        $business->image = $path.'/'.$imageName;
        $business->save();
        return redirect()->route('business.index')->with('success', 'Successfully saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $business = business::where('id',$id)->with('category')->first();
        //dd($business);
        $categories = Category::get();
        return view('admin.business.edit',compact('business','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'category' => 'required',
            'subcategory'=>'required',
        ]);
        
        if($request->input('action')==="update"){
            $business = business::find($id);
            if($request->hasFile('image')){
                $path = 'users/businesses';
                
                $business->title = $request->title;
                $business->description = $request->description;
                $business->category_id = $request->category;
                $business->subcategory_id = $request->subcategory;
                $imageName = time().'.'.$request->image->extension();  
                $request->image->move(public_path($path), $imageName);

                $business->image = $path.'/'.$imageName;
                $business->update();
                
                
                $success = 'Content has been updated successfully!';
                // return redirect()->route('moments.index',array('moments'=>$moments));
                return Redirect::route('business.index')
                                ->with('success', 'Category has been updated');
                                
            }else{
                $business->title = $request->title;
                $business->description = $request->description;
                $business->category_id = $request->category;
                $business->subcategory_id = $request->subcategory;
                $business->update();
                return redirect()->route('business.index') ->with('success', 'Category has been updated');
            }
        }else{
            return redirect()->route('business.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $business = business::find($id);
        $business->delete();
        return redirect()->back();
    }
}
