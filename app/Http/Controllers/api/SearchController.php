<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Addmember;
use App\Models\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function fetchItemsForSearch(){
        // $items = collect([]);
        $items = [];
        $users = User::get()->toArray();
        $items = array_merge($items, $users);
        $members = Addmember::get()->toArray();
        $items = array_merge($items, $members);
        
        return response()->json(['status' => 200,'items' => $items]);
        
    }
    public function search(Request $request){
        $items= [];
        $users =User::where('fname','like','%'.$request->search.'%')->get()->toArray();
        $items = array_merge($items, $users);
        // $members = Addmember::where('member_name','like','%'.$request->search.'%')->get()->toArray();
        // $items = array_merge($items, $members);
        return response()->json(['status' => 200,'items' => $items]);
    }
}
