<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\ResidenceProfile;
use App\Models\CompanyProfile;
use App\Models\job;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {   
        $users =  User::where('id','!=',$id)->with('CompanyProfile', 'ResidenceProfile','Jobs')->where('role_id', 2)->get();
        // dd($users);
        if($users) {
            return response()->json(['status' => 200,'data' => $users]);
        }else {
            return response()->json(['status' => 400, 'messaage' => 'No user found']);
        }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 1. Get user
        $user = User::with('Roles','CompanyProfile','ResidenceProfile','Categories','Addmembers','Jobs')->find($id);
        // 2. If users not found, send back 404 Not Found
        if (is_null($user)) {
            return response()->json(['message' => 'User not found' , 'status' => 404]);
        }
        
        return response()->json(['user' => $user, 'status' => 200]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        dd($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateProfile(Request $request,$id){
        
        if($request->job=="on"){
            User::where('id',$id)->update([
                'job' => 1,
            ]);
        }else{
            User::where('id',$id)->update([
                'job'=> 0,
            ]);
        }
        if($request->matrimonial=="on"){
            User::where('id',$id)->update([
                'matrimonial' => 1,
            ]);
        }else{
            User::where('id',$id)->update([
                'matrimonial'=> 0,
            ]);
        }
        if($request->hasFile('image')){
            
                $path = 'users/images';
                $imageName = time().'.'.$request->image->extension();  
                $request->image->move(public_path($path), $imageName);
                $image = $path.'/'.$imageName;
                
             
           
            $user = [
                'fname'=>$request->fname,
                'mname'=>$request->mname,
                'lname'=>$request->lname,
                'gender'=>$request->gender,
                'bio'=>$request->bio,
                'dob'=>$request->dob,
                'fathernumber'=>$request->fathernumber,
                'image' => $image,
            ];
            
            
        }else{
            //dd('2');
            $user = [
                'fname'=>$request->fname,
                'mname'=>$request->mname,
                'lname'=>$request->lname,
                'gender'=>$request->gender,
                'bio'=>$request->bio,
                'dob'=>$request->dob,
                'fathernumber'=>$request->fathernumber,
            ];
        } 
            
        
        $residenceProfile = [
            'address'=> $request->residence_address,
            'area' => $request->residence_area,
            'city' => $request->residence_city,
            'pincode' => $request->residence_pincode,
            'primary_phone' => $request->residence_phone,
            

        ];
        $companyProfile = [
            'company_name'=> $request->company_name,
            'company_address' => $request->company_address,
            'office_phone' => $request->company_phone,
            'company_email' => $request->company_email,
            'company_website' => $request->company_website,
        ];
        $job = job::where('user_id',$id)->get();
        if(count($job)!==0){
            job::where('user_id',$id)->update([
            
                'field' => $request['jobfield'],
                'location' => $request['joblocation'],
                'experience' => $request['jobexperience'],
                'ctc' => $request['jobctc'],
            ]);
        }else{
            $jobs = new job();
            $jobs->user_id = $id;
            $jobs->field = $request->jobfield;
            $jobs->location = $request->joblocation;
            $jobs->experience = $request->jobexperience;
            $jobs->ctc = $request->jobctc;
            $jobs->save();
        }
       
        // $job = [
        //     'field' => $request->jobfield,
        //     'location' => $request->joblocation,
        //     'experience' => $request->jobexperience,
        //     'ctc' => $request->jobctc,
        // ];
        User::where('id',$id)->update($user);
      
        //job::where('user_id',$id)->update($job);
        ResidenceProfile::where('user_id', $id)->update($residenceProfile);
        CompanyProfile::where('user_id', $id)->update($companyProfile);
        $user = User::with('CompanyProfile','ResidenceProfile','Categories','Native','Jobs')->find($id);
        return response()->json(['user' => $user,'message'=> 'Updated Successfully', 'status' => 200]);
    }

    public function matrimony(){
        $bride = User::where('gender','female')->where('matrimonial',1)->get();
        $groom = User::where('gender','male')->where('matrimonial',1)->get();
        return response()->json(['bride' => $bride, 'groom'=> $groom, 'status' => 200]);
    }

    public function getJob($id){
        $user = User::where('job',1)->where('id','!=',$id)->with('Jobs')->get();
        return response()->json(['user'=>$user,'status'=>200]);
    }

}
