<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\business;
use App\Models\Category;
use App\Models\Subcategory;
class businessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $business = business::with('category.subcategory.Users')->get();
        if(!empty($business)) {
            return response()->json(['status' => 200,'business' => $business]);
        }else {
            return response()->json(['status' => 400,'message' => 'Not data found']);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $business = Business::with('category','subcategory')->find($id);
        if(!empty($business)) {
            return response()->json(['status' => 200,'business' => $business]);
        }else {
            return response()->json(['status' => 400,'message' => 'Not data found']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCategories($business_id){
        $business = Business::with('category')->find($business_id);
        return response()->json(['status' => 200,'category' => $business->category]);
    }

    public function getSubCategories($category_id){
        $category = Category::with('subcategory')->find($category_id);
    
        return response()->json(['status' => 200,'sub_category' => $category->subcategory]);
    }

    public function getUsersBySubCategories($subcategory_id){
        $subcategory = Subcategory::with('users.CompanyProfile','users.ResidenceProfile','users.Native','users.Addmembers','users.SubCategory')->find($subcategory_id);
        return response()->json(['status' => 200,'users' => $subcategory->users,'subcategory'=>$subcategory]);
    }
    
}
