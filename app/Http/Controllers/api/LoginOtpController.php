<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use GuzzleHttp\Client;
class LoginOtpController extends Controller
{

    public function check() {
        return 'check';
    }
    public function Login(Request $request)
    {   //dd($request->all());
        $phone = $request->phone;
        
        $user = User::where('phone', $request->phone)->first();
        
        
           
        
       

        if(!empty($user)) {

            $apikey = 'c5ddc4bc-a73b-11eb-80ea-0200cd936042';
            $otp =  mt_rand(1000,9999);
            $url = "https://2factor.in/API/V1/$apikey/SMS/$phone/$otp";
           
            $client = new Client(); 
            
            
           
           
            $res = $client->request('GET',$url);
            $statusCode = $res->getStatusCode();
            $otpresponse = $res->getBody()->getContents();   
            
            if(!empty($otp)) {
                User::where('phone', $request->phone)->update([
                    'otp' => $otp,
                ]);
                return response()->json(['otp' => $otp, 'status' => 200,'otpresponse'=> $otpresponse])->setStatusCode(200);
            }
        }else {
            return response()->json(['status' => 400, 'message' => 'Mobile number is not registered']);
        }
    }

    public function otpVerified(Request $request) {

        $user = User::with('CompanyProfile', 'ResidenceProfile', 'Native', 'Categories')->where(['otp' => $request->otp, 'phone' =>$request->phone])->first();
        if(!empty($user)) {
            
            return response()->json(['user' =>  $user, 'message' => 'Welcome back', 'status' => 200])->setStatusCode(200);
        }else {
            return response()->json(['status' => 400, 'message' => 'Wrong OTP']);
      
        }

    }
}
