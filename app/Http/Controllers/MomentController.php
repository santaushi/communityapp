<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
#Models
use App\Models\Moment;
use Illuminate\Support\Facades\Redirect;
class MomentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $moments = Moment::get();
        return view('admin.moments.index',compact('moments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.moments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $path = 'users/moments';
        $moment = new Moment();
        $moment->title = $request->title;
        $moment->description = $request->description;
        

        $imageName = time().'.'.$request->image->extension();  
        $request->image->move(public_path($path), $imageName);

        $moment->image = $path.'/'.$imageName;
        $moment->save();
        // dd($moment);
        return back()->with('success','You have successfully created a moment.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $moments = Moment::where('id',$id)->first();
        return view('admin.moments.edit',compact('moments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        
        if($request->input('action')==="update"){
            $moments = Moment::find($id);
            if($request->hasFile('image')){
                $path = 'users/moments';
                
                $moments->title = $request->title;
                $moments->description = $request->description;
                

                $imageName = time().'.'.$request->image->extension();  
                $request->image->move(public_path($path), $imageName);

                $moments->image = $path.'/'.$imageName;
                $moments->update();
                
                
                $success = 'Content has been updated successfully!';
                // return redirect()->route('moments.index',array('moments'=>$moments));
                return Redirect::route('moments.index')
                                ->with('success', 'Moment has been updated');
                                
            }else{
                $moments->title = $request->title;
                $moments->description = $request->description;
                $moments->update();
                return redirect()->route('moments.index') ->with('success', 'Moment has been updated');
            }
        }else{
            return redirect()->route('moments.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $moment = Moment::find($id);
        $moment->delete();
        return redirect()->route('moments.index')->with('success', 'Moment has been Deleted');
    }
}
