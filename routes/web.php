<?php

use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\SubcategoriesController;
use App\Http\Controllers\Admin\AddmemberController;
use App\Http\Controllers\Admin\EventController;
use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ImportExportController;
use App\Http\Controllers\MomentController;
use App\Http\Controllers\BusinessController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\CommitteeController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Artisan;
use League\Flysystem\Config;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('clear-cache', function() {
    Artisan::call('optimize');
    Artisan::call('config:cache');
    Artisan::call('route:cache');
    Artisan::call('view:clear');
    Artisan::call('cache:clear');
    return 'cleared';
});

// Route::get('/', function () {
//     return view('auth.login');
// });
Route::get('/', [DashboardController::class,'index'] )->middleware('auth');;

Route::get('login', function () {
    return view('auth.login');
});
Route::get('/forgot-password', [ForgotPasswordController::class, 'forgotPassword']);
Route::post('/send-reset-password-link', [ForgotPasswordController::class, 'sendResetPassWordLink']);

Route::get('/reset-password/{token}', [ResetPasswordController::class,'getPassword']);
Route::post('/reset-password', [ResetPasswordController::class, 'updatePassword']);


Auth::routes();
Route::get('logout', [LoginController::class, 'logout'])->name('get_logout');
Route::group(['middleware' =>  ['auth', 'admin']], function() {
    Route::prefix('/dashboard')->group(function () {
        Route::get('/', [DashboardController::class,'index'])->name('admin_dashboard');
        // Route::get('dashboard', [DashboardController::class,'index'])->name('admin_dashboard'); 
        Route::resource('users', UserController::class);
        Route::get('remove_user/{id}', [UserController::class, 'remove_user']);  
        Route::get('user/{id}', [UserController::class, 'edit']);  
        Route::get('create_user', [UserController::class, 'create_user'])->name('create_user'); 
        Route::resource('categories', CategoriesController::class);
        Route::get('remove_categories/{id}', [CategoriesController::class, 'remove_categories'])->name('remove_categories');
        Route::resource('subcategories', SubcategoriesController::class);
        Route::get('show_image/{id}', [SubcategoriesController::class, 'show_image']);
        Route::get('remove_subcategories/{id}', [SubcategoriesController::class, 'remove_subcategories'])->name('remove_subcategories');
        Route::get('add_family/{id}', [AddmemberController::class,'index']);
        Route::post('add_family/{id}', [AddmemberController::class, 'add_family'])->name('add_family');
        
        // Route::get('view_family',[AddmemberController::class, 'showMembers']);
        Route::get('view_family/{id}', [AddmemberController::class,'view_family'])->name('view_family');
        Route::get('remove_member/{id}', [AddmemberController::class,'remove_member']);
        Route::get('member_edit/{id}', [AddmemberController::class,'member_edit'])->name('member_edit');
        Route::put('member_update/{id}', [AddmemberController::class,'member_update'])->name('member_update');
        Route::get('memberedit/{id}',[AddmemberController::class,'MemberEdit']);
        Route::get('member/delete/{id}',[AddmemberController::class,'destroy']);
        Route::resource('news', NewsController::class);
        Route::get('news/delete/{id}',[NewsController::class,'destroy']);
        Route::get('newsList', [NewsController::class, 'newsList'])->name('newsList');
        Route::get('news_remove/{id}', [NewsController::class, 'news_remove'])->name('news_remove');
        Route::get('news_image/{id}', [NewsController::class, 'News_image']);
        Route::resource('event',EventController::class);
        Route::get('event/delete/{id}',[EventController::class, 'destroy']);
        Route::get('remove_event/{id}',[EventController::class, 'remove_event']);
        Route::get('events', [EventController::class, 'event_list'])->name('event_list');
        
        
        Route::get('export-users-csv',[ImportExportController::class, 'exportUsersCSV']);
        Route::get('export-users-pdf',[ImportExportController::class, 'exportUsersPDF']);
        Route::get('export-users-excel',[ImportExportController::class, 'exportUsersExcel']);
        Route::resource('moments',MomentController::class);
        Route::get('moments/delete/{id}',[MomentController::class,'destroy']);
        Route::resource('business',BusinessController::class);
        Route::get('business/delete/{id}',[BusinessController::class,'destroy']);

        Route::get('update-profile/{id}',[UserController::class, 'editProfile']);
        Route::get('memberadd/{id}',[AddmemberController::class, 'memberadd']) ;
        Route::get('editmember/{id}',[AddmemberController::class, 'editmemberapi']);
        // Route::get('update-user/{id}',[UserController::class, 'updateProfile']);
        // dahboard/update-user
        Route::get('categories/delete/{id}',[CategoriesController::class,'destroy']);
        Route::get('subcategories/delete/{id}',[SubcategoriesController::class,'destroy']);
        Route::get('fetchsubcategory/{id}',[CategoriesController::class,'findsubcategory']);
        Route::get('fetchOldsubCategory/{id}',[CategoriesController::class,'findOldsubCategory']);
        Route::get('fetch-category-for-subcategory/{id}',[CategoriesController::class,'findCategoryforSubcategory']);
        Route::get('fetch-business-for-category/{id}',[CategoriesController::class,'findBusinessforCategory']);
        Route::resource('committee',CommitteeController::class);
        Route::get('fetch_users/{id}',[UserController::class,'edit']);
        Route::get('fetchOldCategory-forOldsubcategory/{id}',[SubcategoriesController::class,'findOldcategoryForsubcategory']);
        Route::get('fetchOldbusiness-forOldcategory/{id}',[SubcategoriesController::class,'findOldbusinessForcategory']);
        Route::get('admin/profile',[UserController::class,'admin']);
        Route::put('updateAdminprofile',[UserController::class,'updateProfileofAdmin']);
    });
    
    
});

Route::group(['middleware' =>  ['auth', 'user']], function() {
    Route::get('user_dashboard', [DashboardController::class,'index'])->name('user_dashboard');
    
});

Route::get('test', [TestController::class,'index']);
