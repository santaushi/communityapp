<?php

use App\Http\Controllers\api\EventController;
use App\Http\Controllers\api\LoginOtpController;
use App\Http\Controllers\api\MemberController;
use App\Http\Controllers\api\NewsController;
use App\Http\Controllers\api\ProfileController;
use App\Http\Controllers\api\UserController;
use App\Http\Controllers\api\MomentController;
use App\Http\Controllers\api\businessController;
use App\Http\Controllers\api\SearchController;
use App\Http\Controllers\api\CommitteeController;
use App\Http\Controllers\Admin\DashboardController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::group(['prefix' => 'v1'], function () {
   
    Route::post('login', [LoginOtpController::class, 'login']);
    Route::post('otpVerified', [LoginOtpController::class, 'otpVerified']);
    Route::post('profile/{id}', [ProfileController::class, 'EditUser']);
    Route::post('updateprofile/{id}', [ProfileController::class, 'updateprofile']);
    Route::get('members_list', [MemberController::class, 'index']);
    // Route::resource('users/{id}', [UserController::class]);
   
    Route::get('get-users/{id}', [UserController::class, 'index']);
    Route::get('get-user/{id}', [UserController::class, 'show']);
    Route::post('add_family_member/{id}', [MemberController::class , 'add_family_member'])->name('add_family_member');
    Route::get('family_member_detail/{id}', [MemberController::class, 'family_member_detail']);
    Route::post('edit_family_member_profile/{id}', [MemberController::class, 'editFamilymemberProfile']);
    // Route::post('edit_member_profile/{id}', [MemberController::class, 'updateFamilymember']);
    Route::get('edit_profile/{id}',[MemberController::class, 'edit_family_member_profile']);
    // Route::resource('news', NewsController::class);
    Route::post('news_detail', [NewsController::class, 'news_detail']);
    Route::get('get-news/{id}', [NewsController::class, 'getSingleNews']);
    Route::get('get-news', [NewsController::class, 'index']);
    Route::get('event', [EventController::class, 'index']);
    Route::post('event_detail', [EventController::class, 'event_detail']);
    Route::get('get-moments', [MomentController::class, 'index']);
    Route::get('fetch_family_member/{id}',[MemberController::class, 'fetchFamilymember']);

    Route::post('update-user/{id}',[UserController::class, 'updateProfile']);
    Route::get('all-business', [businessController::class, 'index']);
    Route::get('get-business/{id}', [businessController::class, 'show']);
    Route::get('business-category/{business_id}', [businessController::class, 'getCategories']);
    Route::get('category-sub-categories/{category_id}', [businessController::class, 'getSubCategories']);
    Route::get('sub-category-users/{subcategory_id}', [businessController::class, 'getUsersBySubCategories']);
    Route::post('fetch-all-items-for-search',[SearchController::class, 'fetchItemsForSearch']);
    Route::post('search',[SearchController::class, 'search']);
    Route::get('matrimony',[UserController::class, 'matrimony']);
    Route::get('job/{id}',[UserController::class, 'getJob']);
    Route::get('users/{id}',[UserController::class]); 
    Route::get('get-committee-image',[CommitteeController::class,'index']);
    #User Chart
    
    Route::get('get-users-for-chart',[DashboardController::class,'getUsersForChart']); 

// });
