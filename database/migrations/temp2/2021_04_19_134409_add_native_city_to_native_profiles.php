<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNativeCityToNativeProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('native_profiles', function (Blueprint $table) {
            //
            $table->string('native_city')->nullable()->after('native_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('native_profiles', function (Blueprint $table) {
            //
            $table->dropColumn('native_city');
        });
    }
}
