@extends('admin.layout.app')
@section('title', 'Community App | Login')
@section('content')
<div class="app-container app-theme-white body-tabs-shadow">
    <div class="app-container">
        <div class="h-100 bg-plum-plate bg-animation">
            <div class="d-flex h-100 justify-content-center align-items-center">
                <div class="mx-auto app-login-box col-md-8">
                    <div class="mx-auto mb-3 app-logo-inverse"></div>
                    <div class="mx-auto modal-dialog w-100">
                        <div class="modal-content">
                            <form method="POST" action="{{ route('login') }}"> 
                                @csrf
                                <div class="modal-body">

                                    @if (session('message'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('message') }}
                                        </div>
                                    @endif
                                    @if (session('error'))
                                        <div class="alert alert-danger" role="alert">
                                            {{ session('error') }}
                                        </div>
                                    @endif
                                    
                                    <div class="text-center h5 modal-title">
                                        <img src="{{ asset('images/logo.png')}}" alt="Bidasar" class="img-circle" style="width:40%">
                                        <h4 class="mt-2">
                                            <div>Welcome back,</div>
                                            <span>Please sign in to your account below.</span>
                                        </h4>
                                    </div>
                                        
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <div class="position-relative form-group">
                                                    <div class="group">
                                                    <input name="email" placeholder="Email here..." type="email" class="form-control @error('email') is-invalid @enderror">
                                                    @error('email') 
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror   
                                                   
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="position-relative form-group">
                                                    <input name="password" id="examplePassword" placeholder="Password here..." type="password" class="form-control">
                                                    @error('password')
                                                        <span class="invalid-feedback text-danger" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    <div class="divider"></div>
                                </div>
                                <div class="clearfix modal-footer">
                                    <div class="float-left">
                                        <a href="/forgot-password" class="btn-lg btn btn-link">Recover Password</a>
                                    </div>
                                    <div class="float-right">
                                        <input type="submit" class="btn btn-primary btn-lg" value="login">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
    

