@extends('admin.layout.app')
@section('title', 'Community App | Forgot password')
@section('content')
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-0 form-group row">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<div class="app-container app-theme-white body-tabs-shadow">
    <div class="app-container">
        <div class="h-100 bg-plum-plate bg-animation">
            <div class="d-flex h-100 justify-content-center align-items-center">
                <div class="mx-auto app-login-box col-md-8">
                    <div class="mx-auto mb-3 app-logo-inverse"></div>
                    <div class="mx-auto modal-dialog w-100">
                        <div class="modal-content">
                            <form method="POST" action="/send-reset-password-link"> 
                                @csrf
                                <div class="modal-body">
                                    @if (session('message'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('message') }}
                                        </div>
                                    @endif
                                    @if (session('error'))
                                        <div class="alert alert-danger" role="alert">
                                            {{ session('error') }}
                                        </div>
                                    @endif
                                    
                                    <div class="text-center h5 modal-title">
                                        <img src="{{ asset('images/logo.png')}}" alt="Bidasar" class="img-circle" style="width:40%">
                                        <h4 class="mt-2">
                                            <div>Welcome back,</div>
                                            <span>Reset your password</span>
                                        </h4>
                                    </div>
                                        
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <div class="position-relative form-group">
                                                    <div class="group">
                                                    <input name="email" placeholder="Email here..." type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="clearfix modal-footer">
                                    <div class="float-left">
                                        <a href="/login" class="btn-lg btn btn-link">Login</a>
                                    </div>
                                    <div class="float-right">
                                        <input type="submit" class="btn btn-primary btn-lg" value="Send Password Reset Link">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
