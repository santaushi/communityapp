@extends('admin.layout.app')

@section('title', 'Create User')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-note2 icon-gradient bg-mean-fruit"></i>
                </div>
                <div>Edit Profile
                    <div class="page-title-subheading">Edit user from here.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="tabs-animation">
        <div class="mb-3 card">
            <div class="card-header-tab card-header">
                <div class="col-md-2">
                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                        <i class="header-icon lnr-charts icon-gradient bg-happy-green"> </i> Edit Profile
                    </div>
                </div>
            </div>
            <div class="error-field">
                @if (session('status'))
                    <div class="mb-4 text-sm font-medium text-success">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
            <div class="p-3 d-block card-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="myNewform1" method="post" enctype="multipart/form-data" class="form-horizontal form-material">
                            @csrf
                          
                            @if (Route::is('fetch_users.edit'))
                                @method('PUT')
                            @endif
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">First Name</label>
                                        <div class="col-md-12 border-bottom">
                                            <input type="text" name="fname"
                                                value="{{$user->fname}}"
                                                placeholder="John" class="p-0 border-0 form-control">
                                        </div>
                                        @error('fname')
                                            <span class="mt-1 text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Middle Name</label>
                                        <div class="col-md-12 border-bottom">
                                            <input type="text" name="mname"
                                                value="{{ $user->mname }}"
                                                placeholder="Doe" class="p-0 border-0 form-control">
                                        </div>
                                        @error('mname')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Last Name</label>
                                        <div class="col-md-12 border-bottom">
                                            <input type="text" name="lname"
                                                value="{{ $user->lname }}"
                                                placeholder="Johnathan" class="p-0 border-0 form-control">
                                        </div>
                                        @error('lname')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Date Of Birth</label>
                                        <div class="col-md-12 border-bottom">
                                            
                                            <input name="dob"
                                                value="{{ old('$user->dob', date('m/d/y')) }}"
                                               
                                                placeholder="Selected date D.O.B" type="date"
                                                class="datepicker form-control">
                                        </div>
                                        @error('lname')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label for="image" id="image" class="p-0 col-md-12">Image</label>
                                        <div class="col-md-12 border-bottom">
                                           
                                            <input class="form-control" type="file" name="image" id="image">
                                        </div>
                                        @error('lname')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Select Gender</label>
                                        <div class="col-md-12">
                                            <select name="gender" id="" class="form-control">
                                                {{-- <option
                                                    value="{{ Route::is('users.edit') ? $useredit->gender : old('gender') }}">
                                                    {{ Route::is('users.edit') ? $useredit->gender : '-- select value--' }}
                                                </option> --}}
                                                <option>---select value---</option>
                                                <option value="male" {{old('gender',$user->gender) =="male" ? 'selected' : ''}}>Male</option>
                                                <option value="female" {{old('gender',$user->gender) =="female" ? 'selected' : ''}}>Female</option>
                                            </select>
                                        </div>
                                        @error('gender')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Father Number</label>
                                        <div class="col-md-12">
                                            <input type="text" onkeypress="return /^-?\d*$/.test(event.key)" name="fathernumber"
                                            value="{{  $user->fathernumber  }}"
                                                maxlength="10" placeholder="XXXXXXXXXX" class="form-control">
                                        </div>
                                        @error('fathernumber')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group form-check">
                                        {{-- <label class="p-0 col-md-12">Job</label> --}}
                                        <div class="col-md-12 mt-4">
                                            <input type="checkbox" name="job" id="flexCheckChecked" class="form-check-input form-control mt-2 " style="width:20px">
                                            <label class="form-check-label mt-3 ml-3" for="flexCheckChecked" style="font-size: 15px">Job</label>
                                        </div>
                                        @error('job')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group form-check">
                                        {{-- <label class="p-0 col-md-12">Job</label> --}}
                                        <div class="col-md-12 mt-4">
                                            <input type="checkbox" name="matrimonial" id="flexCheckChecked" class="form-check-input form-control mt-2 " style="width:20px">
                                            <label class="form-check-label mt-3 ml-3" for="flexCheckChecked" style="font-size: 15px">Matrimonial</label>
                                        </div>
                                        @error('matrimonial')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                {{-- <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Phone Number</label>
                                        <div class="col-md-12 border-bottom">
                                            <input type="text" name="phone"
                                                value="{{ $user->phone }}"
                                                placeholder="Phone number" class="p-0 border-0 form-control">
                                        </div>
                                        @error('lname')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div> --}}
                                {{-- <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Bio</label>
                                        <div class="col-md-12 border-bottom">
                                            <input type="text" name="bio"
                                                value="{{ $user->bio }}"
                                                placeholder="Bio" class="p-0 border-0 form-control">
                                        </div>
                                        @error('lname')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div> --}}
                               

                            </div>
                            @if($user->ResidenceProfile)
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="bg-white page-breadcrumb">
                                            <h4 class="page-title">Residence Profile</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="mb-4 form-group">
                                            <label class="p-0 col-md-12">Residence Address</label>
                                            <div class="col-md-12 border-bottom">
                                                <input type="text" name="residence_address"
                                                    value="{{ $user->ResidenceProfile->address }}"
                                                    placeholder="Residence Address" class="p-0 border-0 form-control">
                                            </div>
                                            @error('address')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-4">
                                        <div class="mb-4 form-group">
                                            <label class="p-0 col-md-12">Residence Area</label>
                                            <div class="col-md-12 border-bottom">
                                                <input type="text" name="area"
                                                    value="{{ $user->ResidenceProfile->area }}"
                                                    placeholder="Residence Area" class="p-0 border-0 form-control">
                                            </div>
                                            @error('area')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div> --}}
                                    <div class="col-md-4">
                                        <div class="mb-4 form-group">
                                            <label class="p-0 col-md-12">Residence City</label>
                                            <div class="col-md-12 border-bottom">
                                                <input type="text" name="residence_city"
                                                    value="{{ $user->ResidenceProfile->city }}"
                                                    placeholder="Residence City" class="p-0 border-0 form-control">
                                            </div>
                                            @error('city')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-4 form-group">
                                            <label class="p-0 col-md-12">Residence Pincode</label>
                                            <div class="col-md-12 border-bottom">
                                                <input type="text" name="residence_pincode" maxlength="6" minlength = "6"
                                                    value=" {{ $user->ResidenceProfile->pincode }}"
                                                    placeholder="Residence Pincode" class="p-0 border-0 form-control">
                                            </div>
                                            @error('pincode')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-4 form-group">
                                            <label class="p-0 col-md-12">Residence Phone</label>
                                            <div class="col-md-12 border-bottom">
                                                <input type="text" onkeypress="return /^-?\d*$/.test(event.key)" maxlength="10" name="residence_phone"
                                                    {{-- value="{{ Route::is('dashboard/update-profile/{id}') ? $user->ResidenceProfile->primary_phone : old('primary_phone') }}" --}}
                                                    value = {{$user->ResidenceProfile->primary_phone}}
                                                    placeholder="Residence Primary Phone" class="p-0 border-0 form-control">
                                            </div>
                                            @error('primary_phone')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                  
                                </div>
                            @endif

                            
                            @if($user->Native)
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="bg-white page-breadcrumb">
                                            <h4 class="page-title">Company Profile</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="mb-4 form-group">
                                            <label class="p-0 col-md-12">Company Name</label>
                                            <div class="col-md-12 border-bottom">
                                                <input type="text" name="company_name" value="{{ $user->CompanyProfile->company_name}}" placeholder="Company Name"
                                                    class="p-0 border-0 form-control">
                                            </div>
                                            @error('company_name')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-4 form-group">
                                            <label class="p-0 col-md-12">Company Address</label>
                                            <div class="col-md-12 border-bottom">
                                                <input type="text" name="company_address" value="{{ $user->CompanyProfile->company_address }}" placeholder="Address"
                                                    class="p-0 border-0 form-control">
                                            </div>
                                            @error('company_address')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-4 form-group">
                                            <label class="p-0 col-md-12">Company Phone</label>
                                            <div class="col-md-12 border-bottom">
                                                <input type="phone" name="company_phone" value="{{$user->CompanyProfile->office_phone }}" placeholder="Phone"
                                                    class="p-0 border-0 form-control">
                                            </div>
                                            @error('company_phone')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                      
                                    {{-- <div class="col-md-4">
                                        <div class="mb-4 form-group">
                                            <label class="p-0 col-md-12">Is Committee member </label>
                                            <div class="row">
                                                <div class="col-md-6 border-bottom">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label class="p-0 col-md-12">yes </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="checkbox" name="committe_member"  checked
                                                                value="{{ 'checked' ? 1 : 0 }}" class="form-control1">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @error('committe_member')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="mb-4 form-group">
                                            <label class="p-0 col-md-12">Company Email</label>
                                            <div class="col-md-12 border-bottom">
                                                <input type="email" name="company_email" value="{{ $user->CompanyProfile->company_email }}" placeholder="Company Email"
                                                    class="p-0 border-0 form-control">
                                            </div>
                                            @error('company_email')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-4 form-group">
                                            <label class="p-0 col-md-12">Company Website</label>
                                            <div class="col-md-12 border-bottom">
                                                <input type="url" name="company_website" value="{{ $user->CompanyProfile->company_website }}" placeholder="Company website"
                                                    class="p-0 border-0 form-control">
                                            </div>
                                            @error('company_website')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>  
                            @endif
                            <div class="mb-4 form-group">
                                <div class="col-sm-12">
                                    <button type="submit" value="update" name="action" 
                                        class="btn btn-success register">Update</button>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





@endsection
@push('js')
<script>
    $(document).ready(function () {   
   
        $("#myNewform1").on('submit',function (event) {
            event.preventDefault();
            $userId = "{{$user->id}}"
            console.log($userId);
            $.ajax({
               
            url: "/api/update-user/" + $userId,
            method: "POST",
            data: new FormData(this),
            dataType:  "JSON",
            contentType: false,
            cache: false,
            processData: false,
            }).done(function (data) {
                console.log(data);
            });
        });    
            

});
</script>
@endpush