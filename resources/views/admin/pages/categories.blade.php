@extends('admin.layout.app')
@section('title', 'Categories')

@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-display2 icon-gradient bg-mean-fruit"></i>
                </div>
                <div>Categories
                    <div class="page-title-subheading">{{Route::is('categories.edit')?'Update Categories here':'Add Categories Here'}}</div>
                </div>
                
            </div>
            <div class="col-md-9 text-right" id="addbuttoncategory" aria-hidden="true" style="display:block">
                <button class="btn btn-success">{{Route::is('categories.edit')?'-':'+'}}</button>
                
            </div>
        </div>
    </div>

    <div class="mt-4 container-fluid">
        <div class="row form-group">
            <div class="col-lg-12 col-xlg-9 col-md-12">
                        @if (session('status'))
                            <div class="mb-4 font-medium text-sm text-success alert alert-success alert-dismissible fade show">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ session('status') }}
                            </div>
                        @endif
                <div class="mb-3 card" id="categoryform" style="{{Route::is('categories.edit') ? '' :'display:none' }}">
                    <div class="card-header"> 
                        <div class="col-md-10">
                            {{Route::is('categories.edit')? 'Update Categories' :'Add Categories'}} 
                        </div>
                        @if(Route::is('categories.edit'))
                            <div class="col-md-2 text-right">
                                <a class="btn btn-success" href="/dashboard/categories">Add Category</a>
                            </div>
                        @endif 
                    </div>
                    <div class="p-3 d-block card-body">
                        <form id="categoriesform" action="{{ Route::is('categories.edit') ? Route('categories.update', $categories_edit->id) : Route('categories.index') }}" method="post" enctype="multipart/form-data" class="form-horizontal form-material">
                            @csrf
                            @if (Route::is('categories.edit'))
                            @method('PUT')
                            @endif
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Category Name</label>
                                        <div class="col-md-12">
                                            <input type="text" id="categories_name" name="categories_name" value="{{ Route::is('categories.edit') ? $categories_edit->categories_name : '' }}" placeholder="Category Name" class="form-control">
                                        </div>
                                        @error('categories_name')
                                        <span id="error" class="mt-1 text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div  @if(Route::is('categories.edit')) class="col-md-5" @endif class="col-md-8">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Categories Image</label>
                                        <div class="col-md-12">
                                            <input type="file" id="categoryimage" name="categories_image" class="dropify form-control">
                                        </div>
                                        @error('categories_image')
                                        <span id="error" class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div @if(Route::is('categories.edit')) class="col-md-3" @endif >
                                    <div class="mb-4 form-group">
                                        <label class="mt-3 col-md-12"></label>
                                        @if(Route::is('categories.edit') ? Route('categories.edit', $categories_edit->id) : '')
                                        <div class="col-sm-12">
                                            <img src="{{ asset($categories_edit->categories_image) }}"  height="200px" width="100%" class="mr-2" alt="image">
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            
                            </div>
                        
                            <div class="row">
                                
                                <div class="col">
                                    <div class="mb-4 form-group text-center">
                                        <label class="mt-3 col-md-12"></label>
                                    
                                        <div class="col-sm-12 text-center">
                                            <button type="submit" id="submitbutton" class="btn btn-success" style="{{Route::is('categories.edit')?'':'margin-top: -90px'}}">{{Route::is('categories.edit')?'Update':'Add'}}</button>&nbsp;
                                           
                                            <a  class="btn btn-danger" href="/dashboard/categories" style="{{Route::is('categories.edit')?'':'margin-top: -90px'}}">Cancel</a>
                                            
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-12 col-xlg-9 col-md-12">
                <div class="white-box">
                    <div class="mb-3 card">
                        
                        @if (session('remove'))
                            <div class="mb-4 font-medium text-sm text-success alert alert-success alert-dismissible fade show">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ session('remove') }}
                            </div>
                        @endif
                        <div class="card-header">Category List</div>
                        <div class="p-3 d-block card-body">
                            
                            <div class="table-responsive">
                                <table id="categoryTable" class="table table-bordered table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center border-top-0">S.no</th>
                                            <th class="text-center border-top-0">Categories Name</th>
                                            <th class="text-center border-top-0">Categories Image</th>
                                            <th class="text-center border-top-0">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($categories as $category)
                                        <tr  id="categorie_{{ $category->id }}">
                                            <td class="text-center">{{$loop->iteration}}</td>
                                            <td class="text-center">{{ $category->categories_name }}</td>
                                            <td class="text-center">
                                                <img src="{{ asset($category->categories_image) }}" width="100" height="70" class="mr-2" alt="image">
                                            <td class="text-center dropdown">
                                                <button class="btn btn-success" type="button" data-toggle="dropdown" ><i class="fa fa-ellipsis-v"></i> </button> 
                                                <div class="text-center dropdown-menu" style="min-width:75px">
                                                    <a  id="editforcategory" class="dropdown-item" href="{{ Route('categories.edit', $category->id) }}" target="__self" title="Edit" style="cursor: pointer">
                                                    <svg height="18" viewBox="0 0 512 511" width="18" xmlns="http://www.w3.org/2000/svg"><path d="m362.667969 512.484375h-298.667969c-35.285156 0-64-28.714844-64-64v-298.667969c0-35.285156 28.714844-64 64-64h170.667969c11.796875 0 21.332031 9.558594 21.332031 21.335938 0 11.773437-9.535156 21.332031-21.332031 21.332031h-170.667969c-11.777344 0-21.332031 9.578125-21.332031 21.332031v298.667969c0 11.753906 9.554687 21.332031 21.332031 21.332031h298.667969c11.773437 0 21.332031-9.578125 21.332031-21.332031v-170.667969c0-11.773437 9.535156-21.332031 21.332031-21.332031s21.335938 9.558594 21.335938 21.332031v170.667969c0 35.285156-28.714844 64-64 64zm0 0" fill="#607d8b"/><g fill="#42a5f5"><path d="m368.8125 68.261719-168.792969 168.789062c-1.492187 1.492188-2.496093 3.390625-2.921875 5.4375l-15.082031 75.4375c-.703125 3.496094.40625 7.101563 2.921875 9.640625 2.027344 2.027344 4.757812 3.113282 7.554688 3.113282.679687 0 1.386718-.0625 2.089843-.210938l75.414063-15.082031c2.089844-.429688 3.988281-1.429688 5.460937-2.925781l168.789063-168.789063zm0 0"/><path d="m496.382812 16.101562c-20.796874-20.800781-54.632812-20.800781-75.414062 0l-29.523438 29.523438 75.414063 75.414062 29.523437-29.527343c10.070313-10.046875 15.617188-23.445313 15.617188-37.695313s-5.546875-27.648437-15.617188-37.714844zm0 0"/></g></svg><span>&nbsp Edit</span></a></li>
                                                    
                                                    <a type="submit"  class="dropdown-item" title="Delete" style="cursor: pointer" href="{{"/dashboard/categories/delete/".$category->id}}" class="category_delete">
                                                    <svg height="18" viewBox="0 0 512 512" width="18" xmlns="http://www.w3.org/2000/svg"><path d="m256 80h-32v-48h-64v48h-32v-80h128zm0 0" fill="#62808c"/><path d="m304 512h-224c-26.507812 0-48-21.492188-48-48v-336h320v336c0 26.507812-21.492188 48-48 48zm0 0" fill="#e76e54"/><path d="m384 160h-384v-64c0-17.671875 14.328125-32 32-32h320c17.671875 0 32 14.328125 32 32zm0 0" fill="#77959e"/><path d="m260 260c-6.246094-6.246094-16.375-6.246094-22.625 0l-41.375 41.375-41.375-41.375c-6.25-6.246094-16.378906-6.246094-22.625 0s-6.246094 16.375 0 22.625l41.375 41.375-41.375 41.375c-6.246094 6.25-6.246094 16.378906 0 22.625s16.375 6.246094 22.625 0l41.375-41.375 41.375 41.375c6.25 6.246094 16.378906 6.246094 22.625 0s6.246094-16.375 0-22.625l-41.375-41.375 41.375-41.375c6.246094-6.25 6.246094-16.378906 0-22.625zm0 0" fill="#fff"/></svg><span>&nbsp Delete</span></a></li>
                                                </div> 
                                                
                                            </td>
                                        
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script>
        
        
        $('#categoryTable').DataTable();
        $('.dropify').dropify(); 
        
        $('#addbuttoncategory').on('click',function(){
            $('#categoryform').toggle();
            
            if($(this).find("button").html()==="+"){
                $(this).find("button").html('-');
            }else{
                $(this).find("button").html('+');
            }
            
        })
       
      
        $(window).on('load',function(){
            var message = $('#error').text();
           
            if(message!==''){
               $('#addbuttoncategory').find('button').html('-');
               $('#categoryform').show();
              
            }
            
        })
        
        $('#editforcategory').on('click',function(){
            
            $('#categoryform').show();
        })
       
    </script>
@endpush