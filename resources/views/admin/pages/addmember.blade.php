@extends('admin.layout.app')

@section('title', 'Add Member')

@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-user icon-gradient bg-mean-fruit"></i>
                </div>
                <div>Add Member
                    <div class="page-title-subheading">Add Member </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="tabs-animation">
        <div class="mb-3 card">
           
            <div class="card-header">
                <div class="col-md-3">
                    Add Member
                </div>
                <div class="col-md-9 text-right">
                    <a href="{{url('/dashboard/view_family/'. $user_id)}}" class="btn btn-success" style="color:white">View Family</a>
                </div>
            </div>

            <div class="p-3 d-block card-body">
                <form action="{{ Route::is('/dashboard/member_edit') ? Route('member_update',$member_edit->id )  : url('/dashboard/add_family/{id}')}}" method="POST" enctype="multipart/form-data" class="form-horizontal form-material">
                    {{-- <form action="/api/add_family_member/{{$user_id}}" method="POST" enctype="multipart/form-data" class="form-horizontal form-material">      --}}
                        @csrf
                        @if (Route::is('/dashboard/member_edit'))
                        @method('PUT')
                        @endif
                        <div class="row">
                            <input type="hidden" name="user_id" value="{{ $user_id }}" placeholder="John" class="p-0 border-0 form-control">
                            
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12"> Name</label>
                                    <div class="col-md-12">
                                        <input type="text" name="member_name" value="{{ Route::is('member_edit') ? $member_edit->member_name :  old('member_name') }}" placeholder="Name" class="form-control">
                                    </div>
                                    @error('member_name')
                                    <span class="mt-1 text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">phone</label>
                                    <div class="col-md-12">
                                        <input type="text" name="member_phone" maxlength="13" value="{{ Route::is('member_edit') ? $member_edit->member_phone : old('member_phone') }}" placeholder="Phone Number" class="form-control">
                                    </div>
                                    @error('member_phone')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">Address</label>
                                    <div class="col-md-12">
                                        <input type="text" name="member_address" value="{{ Route::is('member_edit') ? $member_edit->member_address : old('member_address') }}" placeholder="Address" class="form-control">
                                    </div>
                                    @error('member_address')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">City</label>
                                    <div class="col-md-12">
                                        <input type="text" name="member_city" value="{{ Route::is('member_edit') ? $member_edit->member_city : old('member_city') }}" placeholder="City" class="form-control">
                                    </div>
                                    @error('member_city')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">Pin code</label>
                                    <div class="col-md-12">
                                        <input type="text" name="member_pincode" maxlength="6" value="{{ Route::is('member_edit') ? $member_edit->member_pincode : old('member_pincode') }}" placeholder="Pin Code" class="form-control">
                                    </div>
                                    @error('member_pincode')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">Select Gender</label>
                                    <div class="col-md-12 border-bottom">
                                        <select name="member_gender" id="" class="form-control">
                                            
                                            <option value="{{ Route::is('member_edit') ? $member_edit->member_gender : '' }}">{{ Route::is('member_edit') ? $member_edit->member_gender : '-- select gender--' }}</option>

                                            {{-- <option value="{{ Route::is('fetch_users.edit') ? $useredit->gender : '' }}">{{ Route::is('fetch_users.edit') ? $useredit->gender : '-- select value--' }}</option> --}}
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                    @error('member_gender')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">Image</label>
                                    <div class="col-md-12 border-bottom">
                                        <input type="file" name="member_image"  class="p-0 border-0 form-control">
                                    </div>
                                    @error('member_image')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">Select Relation</label>
                                    <div class="col-md-12 border-bottom">
                                        <select name="member_relation" id="" class="form-control">
                                            <option value="{{ Route::is('member_edit') ? $member_edit->member_relation : '' }}">{{ Route::is('member_edit') ? $member_edit->member_relation : '-- select Relation--' }}</option>
                                            <option value="brother">Brother</option>
                                            <option value="father">Father</option>
                                        </select>
                                    </div>
                                    @error('member_relation')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="mb-4 form-group">
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-success"> {{ Route::is('member_edit')  ? 'Update' : 'Add'}}</button>
                                <a  class="btn btn-danger" href="{{Route('view_family',$user_id)}}"> Cancel</a>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

@endsection
