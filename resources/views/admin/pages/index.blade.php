@extends('admin.layout.app')
@section('title')
    Dashboard
@endsection
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-graph1 icon-gradient bg-mean-fruit"></i>
                    </div> 
                    <div>
                        <div class="page-title-subheading">This is an Admin dashboard.</div>
                    </div>
                </div> 
            </div>
        </div> 
    <div class="tabs-animation">
        <div class="mb-3 card">
            <div class="card-header-tab card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal"><i class="header-icon lnr-charts icon-gradient bg-happy-green"></i>Application Performance
                </div> 
                {{-- <div class="btn-actions-pane-right text-capitalize">
                    <button class="btn-wide btn-outline-2x mr-md-2 btn btn-outline-focus btn-sm">View All</button>
                </div> --}}
            </div> 
            <div class="no-gutters row">
                <div class="col-sm-6 col-md-4 col-xl-4">
                    <div class="text-left bg-transparent card no-shadow rm-border widget-chart">
                        <div class="icon-wrapper rounded-circle">
                            <div class="icon-wrapper-bg opacity-10 bg-warning"></div> 
                            <i class="pe-7s-users text-dark opacity-8"></i>
                        </div> 
                        <div class="widget-chart-content">
                            <div class="widget-subheading">Total Users</div> 
                            <div class="widget-numbers">{{$usercount}}</div> 
                            {{-- <div class="widget-description opacity-8 text-focus">
                                <div class="pr-1 d-inline text-danger"><i class="fa fa-angle-down"></i> <span class="pl-1">54.1%</span></div>less earnings
                            </div> --}}
                        </div>
                    </div> 
                    <div class="m-0 divider d-md-none d-sm-block"></div>
                </div> 
                <div class="col-sm-6 col-md-4 col-xl-4">
                    <div class="text-left bg-transparent card no-shadow rm-border widget-chart">
                        <div class="icon-wrapper rounded-circle">
                            <div class="icon-wrapper-bg opacity-9 bg-danger"></div> <i class="text-white lnr-graduation-hat"></i></div> <div class="widget-chart-content">
                                <div class="widget-subheading">Total Events</div> 
                                <div class="widget-numbers"><span>{{$TotalEvent}}</span></div> 
                                {{-- <div class="widget-description opacity-8 text-focus">Grow Rate:
                                    <span class="pl-1 text-info"><i class="fa fa-angle-down"></i> <span class="pl-1">14.1%</span></span>
                                </div> --}}
                            </div>
                        </div> 
                        <div class="m-0 divider d-md-none d-sm-block"></div>
                    </div> 
                    <div class="col-sm-12 col-md-4 col-xl-4">
                        <div class="text-left bg-transparent card no-shadow rm-border widget-chart">
                            <div class="icon-wrapper rounded-circle">
                                <div class="icon-wrapper-bg opacity-9 bg-success"></div> <i class="text-white lnr-apartment"></i></div> <div class="widget-chart-content"><div class="widget-subheading">Total Visitors</div> 
                                <div class="widget-numbers text-success"><span>$563</span></div> 
                                {{-- <div class="widget-description text-focus">Increased by
                                    <span class="pl-1 text-warning"><i class="fa fa-angle-up"></i> <span class="pl-1">7.35%</span></span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="p-3 text-center d-block card-footer">
                    {{-- <button class="btn-pill btn-shadow btn-wide fsize-1 btn btn-primary btn-lg">
                        <span class="mr-2 opacity-7"><i class="icon icon-anim-pulse ion-ios-analytics-outline"></i></span> 
                    </button> --}}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="mb-3 main-card card">
                    <div class="card-body">
                        <h5 class="card-title">Users</h5>
                        <div >
                            <canvas id="users-chart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>

@endsection
@push('js')
<script>
    $.ajax({
        type:'GET',
        url:"{{url('api/get-users-for-chart')}}",        
        
        dataType:'json',//return data will be json
        success:function(data){
            console.log(data);
            let userCount   = [];
            let monthLables = [];
            for (const property in data) {
                // console.log(`${property}, ${data[property]}`);
                // console.log(data[property])
                userCount.push(data[property].length);
                monthLables.push(property);
            }



            var ctx = document.getElementById('users-chart').getContext('2d');
            
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: monthLables,
                    datasets: [{
                        label: '# Users',
                        data: userCount,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {

                    // xAxes: [{
                    //     display: true,
                    //     scaleLabel: {
                    //     display: true,
                    //     labelString: 'Month'
                    //     }
                    // }],

                    scales: {
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                            display: true,
                            labelString: 'Value'
                            },
                            ticks: {
                            min: 0,

                            // forces step size to be 5 units
                            stepSize: 1 // <----- This prop sets the stepSize
                            }
                        }]
                    }
                }
            });
            },
            error:function(e){
                console.log("inerr");
                console.log(e);
            }
    });


    
    </script>
@endpush