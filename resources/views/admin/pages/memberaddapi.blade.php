@extends('admin.layout.app')

@section('title', 'Add Member')

@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-note2 icon-gradient bg-mean-fruit"></i>
                </div>
                <div>Create Member
                    <div class="page-title-subheading">Create Member </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="tabs-animation">
        <div class="mb-3 card">
            <div class="card-header-tab card-header">
                <div class="row">
                    <div class="col-md-6">
                        @if (session('status'))
                        <div class="mb-4 text-sm font-medium text-success">
                            {{ session('status') }}
                        </div>
                        @elseif(!session('status'))
                            <div class="card-header-title font-size-lg text-capitalize font-weight-normal"><i class="header-icon lnr-charts icon-gradient bg-happy-green"></i>Create Member
                            </div> 
                        @endif
                    </div>
                    <div class="col-md-6">
                        <a href="{{url('/dashboard/view_family/'. $user_id)}}" class="btn btn-primary" style="color:white">View Family</a>
                    </div>
                </div>
               
                <div>
                   
                </div>
            </div> 

            <div class="p-3 d-block card-body">

                    <form id="myform" name="myform" enctype="multipart/form-data" method="post">
                        @csrf
                        
                        <div class="row">
                            <input type="hidden" name="user_id" value="{{ $user_id }}" placeholder="John" class="p-0 border-0 form-control">
                            
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12"> Name</label>
                                    <div class="col-md-12 border-bottom">
                                        <input type="text" name="member_name" id="membername" value="{{ Route::is('member_edit') ? $member_edit->member_name :  old('member_name') }}" placeholder="John" class="p-0 border-0 form-control">
                                    </div>
                                    @error('member_name')
                                    <span class="mt-1 text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">phone</label>
                                    <div class="col-md-12 border-bottom">
                                        <input type="text" name="member_phone" id="memberphone" maxlength="13" value="{{ Route::is('member_edit') ? $member_edit->member_phone : old('member_phone') }}" placeholder="1234567890" class="p-0 border-0 form-control">
                                    </div>
                                    @error('member_phone')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">Address</label>
                                    <div class="col-md-12 border-bottom">
                                        <input type="text" name="member_address" id="memberaddress" value="{{ Route::is('member_edit') ? $member_edit->member_address : old('member_address') }}" placeholder="Johnathan" class="p-0 border-0 form-control">
                                    </div>
                                    @error('member_address')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">City</label>
                                    <div class="col-md-12 border-bottom">
                                        <input type="text" name="member_city" id="membercity" value="{{ Route::is('member_edit') ? $member_edit->member_city : old('member_city') }}" placeholder="Jaipur" class="p-0 border-0 form-control">
                                    </div>
                                    @error('member_city')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">Pin code</label>
                                    <div class="col-md-12 border-bottom">
                                        <input type="text" name="member_pincode" id="memberpincode" maxlength="6" value="{{ Route::is('member_edit') ? $member_edit->member_pincode : old('member_pincode') }}" placeholder="123456" class="p-0 border-0 form-control">
                                    </div>
                                    @error('member_pincode')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">Select Gender</label>
                                    <div class="col-md-12 border-bottom">
                                        <select name="member_gender" id="membergender" class="form-control">
                                            
                                            <option value="{{ Route::is('member_edit') ? $member_edit->member_gender : '' }}">{{ Route::is('member_edit') ? $member_edit->member_gender : '-- select gender--' }}</option>

                                            {{-- <option value="{{ Route::is('fetch_users.edit') ? $useredit->gender : '' }}">{{ Route::is('fetch_users.edit') ? $useredit->gender : '-- select value--' }}</option> --}}
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                    @error('member_gender')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">Image</label>
                                    <div class="col-md-12 border-bottom">
                                        <input type="file" name="member_image" id="memberimage" class="p-0 border-0 form-control">
                                    </div>
                                    @error('member_image')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">Select Relation</label>
                                    <div class="col-md-12 border-bottom">
                                        <select name="member_relation" id="memberrelation" class="form-control">
                                            <option value="{{ Route::is('member_edit') ? $member_edit->member_relation : '' }}">{{ Route::is('member_edit') ? $member_edit->member_relation : '-- select Relation--' }}</option>
                                            <option value="brother">Brother</option>
                                            <option value="father">Father</option>
                                        </select>
                                    </div>
                                    @error('member_relation')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="mb-4 form-group">
                            <div class="col-sm-12">
                                {{-- <button type="submit" class="btn btn-success"> {{ Route::is('member_edit')  ? 'Update' : 'Add'}}</button> --}}
                                <button type="submit" class="btn btn-success" id="submit" > Add</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

@endsection
@push('js')
<script>
 $(document).ready(function () {   
   
    $("#myform").on('submit',function (event) {
        event.preventDefault();
        
        $.ajax({
       
        url: "/api/add_family_member/{{$user_id}}",
        method: "POST",
        data: new FormData(this),
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        }).done(function (data) {
            console.log(data);
        });
    });    
       

});
</script>
@endpush