@extends('admin.layout.app')

@section('title', 'Event')

@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-date icon-gradient bg-mean-fruit"></i>
                </div>
                <div>Event
                    <div class="page-title-subheading">{{Route::is('event.edit')?'Update Event Here':'Add Event Here'}}</div>
                </div>
                
            </div>
        </div>
    </div>
   
    <div class="container-fluid mt-4">
        <div class="row">
            <div class="col-lg-12 col-xlg-9 col-md-12">
                @if (session('status'))
                    <div class="mb-4 font-medium text-sm text-success alert alert-success alert-dismissible fade show">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-10">{{Route::is('event.edit')?'Update Event':'Add Event'}}</div>
                        <div class="col-md-2 text-right">
                            <a href="{{ Route('event_list') }}" class="btn btn-sm btn-success text-white">List Of Event</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ Route::is('event.edit') ? Route('event.update', $event_edit->id) : Route('event.index') }}" method="POST" enctype="multipart/form-data" class="form-horizontal form-material">
                            @csrf
                            @if (Route::is('event.edit'))
                            @method('PUT')
                            @endif
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Event Name</label>
                                        <div class="col-md-12">
                                            <input type="text" name="name" value="{{ Route::is('event.edit') ? $event_edit->name :  old('name') }}" placeholder="Event name" class="form-control">
                                        </div>
                                        @error('name')
                                        <span class="text-danger mt-1">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Event place</label>
                                        <div class="col-md-12">
                                            <input type="text" name="place" value="{{ Route::is('event.edit') ? $event_edit->place :  old('place') }}" placeholder="Event Place" class="form-control">
                                        </div>
                                        @error('place')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Event Start Date</label>
                                        <div class="col-md-12">
                                            <input name="date"
                                            value="{{ Route::is('event.edit') ? $event_edit->date :  old('date') }}"
                                                placeholder="Selected Event date" type="date"
                                                class="datepicker form-control border-no">
                                        </div>
                                        @error('date')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Event End Date</label>
                                        <div class="col-md-12">
                                            <input name="enddate"
                                            value="{{ Route::is('event.edit') ? $event_edit->enddate :  old('enddate') }}"
                                                placeholder="Selected Event date" type="date"
                                                class="datepicker form-control border-no">
                                        </div>
                                        @error('enddate')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Longitude</label>
                                        <div class="col-md-12 ">
                                            <input type="text"  name="long" value="{{ Route::is('event.edit') ? $event_edit->long :  old('long') }}" placeholder="Enter Longitute " class="form-control">
                                        </div>
                                        @error('long')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Latitude</label>
                                        <div class="col-md-12 ">
                                            <input type="text"  name="lat" value="{{ Route::is('event.edit') ? $event_edit->lat :  old('lat') }}" placeholder="Enter Langitute" class="form-control ">
                                        </div>
                                        @error('lat')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Event Time</label>
                                        <div class="col-md-12">
                                            <input type="time" name="time" value="{{ Route::is('event.edit') ? $event_edit->time :  old('time') }}" placeholder="Select time" class="form-control">
                                        </div>
                                        @error('time')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Guest Charge</label>
                                        <div class="col-md-12">
                                            <input type="text" name="charge"  value="{{ Route::is('event.edit') ? $event_edit->charge : old('charge') }}" placeholder="short description" class="form-control">
                                        </div>
                                        @error('charge')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                            
                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0" for="image">Upload Event Image</label>
                                        <div class="col-md-12 border-bottom">
                                            <input type="file" name="image" id="image" class="dropify">
                                        </div>
                                        @error('image')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Long Description</label>
                                        <div class="col-md-12 border-bottom">
                                            <textarea name="description" id="summernoteevent">{{ Route::is('event.edit') ? $event_edit->description :  old('description') }}</textarea>
                                        </div>
                                        @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div> 
                                
                            </div>
                        
                            <div class="form-group">
                            
                                
                                <div class="col-sm-12 text-center">
                                    <button type="submit" class="btn btn-success"> {{ Route::is('event.edit')  ? 'Update' : 'Add'}}</button>&nbsp;
                                    
                                    <a class="btn btn-danger" href="/dashboard/events">Cancel</a>
                                   
                                </div>
                            
                            </div>
                        
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script>
        $('.dropify').dropify({
            height: 195,
        });
        $('#summernoteevent').summernote({
            height: 100,
        });
    </script>
@endpush