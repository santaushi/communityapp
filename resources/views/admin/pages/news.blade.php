@extends('admin.layout.app')

@section('title', 'News')

@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-news-paper icon-gradient bg-mean-fruit"></i>
                </div>
                <div>News
                    <div class="page-title-subheading">{{Route::is('news.edit')?'Update News Here':'Add News Here'}}</div>
                </div>
                
            </div>
        </div>
    </div>

{{-- <div class="page-breadcrumb bg-white">
<div class="row align-items-center">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Dashboard</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <div class="d-md-flex">
            <ol class="breadcrumb ms-auto">
                <li><a href="#" class="fw-normal">Admin</a></li>
            </ol>
        </div>
    </div>
</div>
</div>    --}}
{{-- <div class="page-breadcrumb bg-white">
    <div class="row align-items-center">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">News</h4>
            <div class="error-field">
                @if (session('status'))
                <div class="mb-4 font-medium text-sm text-success">
                    {{ session('status') }}
                </div>
                @elseif(!session('status'))
                <div class="form-group mb-4">
                    <div class="mb-4 font-medium text-sm text-success">
                        Add News
                    </div>
                </div>
                
                @endif
            </div>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <div class="d-md-flex">
                <ol class="breadcrumb ms-auto">
                    <li>
                        <div class="error-field">
                            <div class="form-group mb-4">
                                <div class="col-sm-12">
                                    <a href="{{ url('newsList') }}" class="btn btn-sm btn-success text-white">List news</a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div> --}}
    <div class="container-fluid ">
        <div class="row">
            <div class="col-lg-12 col-xl-12 col-md-12" style="overflow-x: hidden">
                @if (session('status'))
                    <div class="mb-4 font-medium text-sm text-success alert alert-success alert-dismissible fade show">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card mb-4">
                    <div class="card-header">
                    
                        <div class="col-md-10">{{Route::is('news.edit')?'Update News':'Add News'}}</div>
                        <div class="col-md-2 text-right">
                            <a href="/dashboard/newsList" class="btn btn-sm btn-success text-white">News List</a>
                        </div>
                    </div>
                    <div class="p-3 d-block card-body">
                        <form action="{{ Route::is('news.edit') ? Route('news.update', $newsedit->id) : Route('news.index') }}" method="POST" enctype="multipart/form-data" class="form-horizontal form-material">
                        {{-- <form action="/dashboard/news" method="POST" enctype="multipart/form-data" class="form-horizontal form-material"> --}}
                            @csrf
                            @if (Route::is('news.edit'))
                            @method('PUT')
                            @endif
                            <div class="row">
                                
                                <div class="col-md-4">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0"> Title</label>
                                        <div class="col-md-12">
                                            <input type="text" name="title" value="{{ Route::is('news.edit') ? $newsedit->title :  old('title') }}" placeholder="Title" class="form-control">
                                        </div>
                                        @error('title')
                                        <span class="text-danger mt-1">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Short Description</label>
                                        <div class="col-md-12 border-bottom">
                                            <input type="text" name="sort_description"  value="{{ Route::is('news.edit') ? $newsedit->sort_description : old('sort_description') }}" placeholder="short description" class="form-control">
                                        </div>
                                        @error('sort_description')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Place</label>
                                        <div class="col-md-12 border-bottom">
                                            <input type="text" name="place" value="{{ Route::is('news.edit') ? $newsedit->place : old('place') }}" placeholder="News place" class="form-control">
                                        </div>
                                        @error('place')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Date</label>
                                        <div class="col-md-12 border-bottom">
                                            <input name="date"
                                                value="{{ Route::is('news.edit') ? $newsedit->date : old('date') }}"
                                                placeholder="Selected news date" type="date"
                                                class="datepicker form-control border-no">
                                        </div>
                                        @error('date')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Time</label>
                                        <div class="col-md-12 border-bottom">
                                            <input type="time" name="time" value="{{ Route::is('news.edit') ? $newsedit->time : old('time') }}" placeholder="jaipur" class="form-control">
                                        </div>
                                        @error('time')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                
                                <div class="col-md-12">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Long Description</label>
                                        <div class="col-md-12 border-bottom">
                                            <textarea name="long_description" id="summernote">{{ Route::is('news.edit')  ? $newsedit->long_description :  old('long_description') }}</textarea>
                                        </div>
                                        @error('long_description')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group mb-4">
                                        {{-- <label for="image">Image</label>
                                        
                                        <input type="file" name="image[]" id="image" class="dropify" multiple="multiple"> --}}
                                        <label for="image">Image</label>
                                        <input type="file" name="image" id="image" class="dropify">
                                       
                                    </div>
                                    @error('image')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>  

                            </div>
                            <div class="form-group mb-4">
                                <div class="col-sm-12 text-center">
                                    <button type="submit" class="btn btn-success"> {{ Route::is('news.edit')  ? 'Update' : 'Add'}}</button>&nbsp;
                                   
                                    <a class="btn btn-danger" href="/dashboard/newsList">Cancel</a>
                                    
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')

    <script>
          $('.dropify').dropify();
          $('#summernote').summernote({
                
                height: 150,
            });
       
    </script> 
    
@endpush