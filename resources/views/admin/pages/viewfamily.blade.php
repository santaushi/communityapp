@extends('admin.layout.app')
@section('title', 'Member List')

@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-user icon-gradient bg-mean-fruit"></i>
                </div>
                <div>Members
                    <div class="page-title-subheading">Family Members List.</div>
                </div>
            </div>
            <div class="col-md-9 text-right">
                <a class="btn btn-success" href="/dashboard/users">Users List <i class="pe-7s-back pe-lg"></i></a>
            </div>
        </div>
    </div>

    <div class="tabs-animation">
        @if (session('status'))
                    <div class="mb-4 font-medium text-sm text-success alert alert-success alert-dismissible fade show">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ session('status') }}
                    </div>
        @endif
        @if (session('remove'))
                    <div class="mb-4 font-medium text-sm text-success alert alert-danger alert-dismissible fade show">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ session('remove') }}
                    </div>
        @endif
        <div class="mb-3 card">
            <div class="card-header-tab card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal"><i class="header-icon lnr-charts icon-gradient bg-happy-green"></i>All Members
                </div> 
                <div class="btn-actions-pane-right text-capitalize">
                    <a href="{{ url('/dashboard/add_family', $user_id ) }}" class="btn btn-success pull-right">Add Family Member</a>
                </div>
            </div> 

            <div class="p-3 d-block card-body">
                <div class="table-responsive">
                    <table id="memberlistTable" class="table table table-bordered table-hover text-nowrap">
                        <thead>
                            <tr>
                                <th class="border-top-0">S.no</th>
                                <th class="border-top-0">Name</th>
                                <th class="border-top-0">Contact</th>
                                <th class="border-top-0">Relation</th>
                                <th class="border-top-0">Gender</th>
                                <th class="border-top-0">Image</th>
                                <th class="border-top-0">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($members as $member)
                            <tr id ="member_id_{{ $member->id }}">
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{ $member->member_name }}</td>
                                <td>{{ $member->member_phone }}</td>
                                <td>{{ $member->member_relation }}</td>
                                <td>{{ $member->member_gender }}</td>
                                <td><img src="{{asset($member->member_image)}}" class="img-thumbnail img-responsive" width="100px"></td>
                                {{-- <td>
                                    <div class="btn-use">
                                        <ul>
                                            <li title="Edit Record"><a href="{{ url('/dashboard/memberedit', $member->id) }}" title="Edit"><i class="metis-icon pe-7s-edit"></i></a></li>
                                            <li title="Delete Record" user-id="{{ $member->id }}" class="remove-member delete-record"><i class="fas fa-trash text-danger"></i></li>
                                        </ul>
                                    </div>
                                </td> --}}
                                <td class="text-center dropdown">
                                    <button class="btn btn-success" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i> </button> 
                                    <div class="dropdown-menu text-center" style="min-width:75px">
                                        <a  class="dropdown-item" href="{{ url('/dashboard/memberedit', $member->id) }}" target="__self" title="Edit" style="cursor: pointer">
                                        <svg height="18" viewBox="0 0 512 511" width="18" xmlns="http://www.w3.org/2000/svg"><path d="m362.667969 512.484375h-298.667969c-35.285156 0-64-28.714844-64-64v-298.667969c0-35.285156 28.714844-64 64-64h170.667969c11.796875 0 21.332031 9.558594 21.332031 21.335938 0 11.773437-9.535156 21.332031-21.332031 21.332031h-170.667969c-11.777344 0-21.332031 9.578125-21.332031 21.332031v298.667969c0 11.753906 9.554687 21.332031 21.332031 21.332031h298.667969c11.773437 0 21.332031-9.578125 21.332031-21.332031v-170.667969c0-11.773437 9.535156-21.332031 21.332031-21.332031s21.335938 9.558594 21.335938 21.332031v170.667969c0 35.285156-28.714844 64-64 64zm0 0" fill="#607d8b"/><g fill="#42a5f5"><path d="m368.8125 68.261719-168.792969 168.789062c-1.492187 1.492188-2.496093 3.390625-2.921875 5.4375l-15.082031 75.4375c-.703125 3.496094.40625 7.101563 2.921875 9.640625 2.027344 2.027344 4.757812 3.113282 7.554688 3.113282.679687 0 1.386718-.0625 2.089843-.210938l75.414063-15.082031c2.089844-.429688 3.988281-1.429688 5.460937-2.925781l168.789063-168.789063zm0 0"/><path d="m496.382812 16.101562c-20.796874-20.800781-54.632812-20.800781-75.414062 0l-29.523438 29.523438 75.414063 75.414062 29.523437-29.527343c10.070313-10.046875 15.617188-23.445313 15.617188-37.695313s-5.546875-27.648437-15.617188-37.714844zm0 0"/></g></svg><span>&nbsp Edit</span></a></li>
                                        
                                        <a type="submit"  class="dropdown-item" id="memberdel" title="Delete" style="cursor: pointer"  onclick="return confirm('Are you sure? Record will delete permanently')" href="{{ url('/dashboard/member/delete', $member->id) }}">
                                        <svg height="18" viewBox="0 0 512 512" width="18" xmlns="http://www.w3.org/2000/svg"><path d="m256 80h-32v-48h-64v48h-32v-80h128zm0 0" fill="#62808c"/><path d="m304 512h-224c-26.507812 0-48-21.492188-48-48v-336h320v336c0 26.507812-21.492188 48-48 48zm0 0" fill="#e76e54"/><path d="m384 160h-384v-64c0-17.671875 14.328125-32 32-32h320c17.671875 0 32 14.328125 32 32zm0 0" fill="#77959e"/><path d="m260 260c-6.246094-6.246094-16.375-6.246094-22.625 0l-41.375 41.375-41.375-41.375c-6.25-6.246094-16.378906-6.246094-22.625 0s-6.246094 16.375 0 22.625l41.375 41.375-41.375 41.375c-6.246094 6.25-6.246094 16.378906 0 22.625s16.375 6.246094 22.625 0l41.375-41.375 41.375 41.375c6.25 6.246094 16.378906 6.246094 22.625 0s6.246094-16.375 0-22.625l-41.375-41.375 41.375-41.375c6.246094-6.25 6.246094-16.378906 0-22.625zm0 0" fill="#fff"/></svg><span>&nbsp Delete</span></a></li>
                                    </div>    
                                </td>
                               
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




{{-- <div class="mt-4 container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">User List</h3>
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                            <tr>
                                <th class="border-top-0">S.no</th>
                                <th class="border-top-0">Name</th>
                                <th class="border-top-0">Contact</th>
                                <th class="border-top-0">Relation</th>
                                <th class="border-top-0">Gender</th>
                                <th class="border-top-0">Image</th>
                                <th class="border-top-0">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                            @foreach ($members as $member)
                            <tr id ="member_id_{{ $member->id }}">
                                <td>{{ $i++ }}</td>
                                <td>{{ $member->member_name }}</td>
                                <td>{{ $member->member_phone }}</td>
                                <td>{{ $member->member_relation }}</td>
                                <td>{{ $member->member_gender }}</td>
                                <td>{{ $member->member_relation }}</td>
                                <td>
                                    <div class="btn-use">
                                        <ul>
                                            <li title="Edit Record"><a href="{{ url('member_edit', $member->id) }}" title="Edit"><i class="fas fa-eye text-primary"></i></a></li>
                                            <li title="Delete Record" user-id="{{ $member->id }}" class="remove-member delete-record"><i class="fas fa-trash text-danger"></i></li>
                                            
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
@push('js')
<script>
     
     $('#memberlistTable').DataTable();
    
    // $(document).ready(function(){
    //     $('#memberdel').confirmation({
    //         selector:'[data-toggle="confirmation"]'

            
    //     })
    // })
    
    // $(document).ready(function(){
    //     $('#memberdel').on('click',function(){
    //         $(this).attr("data-toggle","popover");
    //         $('[data-toggle="popover"]').popover({
    //             placement : 'top',
    //             html : true,
    //             title : '<>Are you sure?</><div>This record will delete permanently</div>',
    //             content : '<div class="media"><div class="media-body"><h5 class="media-heading">Jhon Carter</h5><p>Excellent Bootstrap popover! I really love it.</p></div></div>'
    //         });
    //     })
        
    // })

    
</script>
@endpush