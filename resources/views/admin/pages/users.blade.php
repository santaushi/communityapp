@extends('admin.layout.app')
@section('title', 'Users')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-user icon-gradient bg-mean-fruit"></i>
                </div>
                <div>Users
                    <div class="page-title-subheading">All users list.</div>
                </div>
            </div>
            
        </div>
    </div>

    <div class="tabs-animation">
        @if (session('status'))
                        <div class="mb-4 text-sm font-medium text-success alert alert-success alert-dismissible fade show">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ session('status') }}
                        </div>
        @endif
        @if (session('remove'))
            <div class="mb-4 text-sm font-medium text-success alert alert-success alert-dismissible fade show">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                {{ session('remove') }}
            </div>
        @endif
        <div class="mb-3 card">
            <div class="card-header-tab card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    <i class="header-icon lnr-charts icon-gradient bg-happy-green"> </i> All Users
                    <div class="text-right col-md-9">
                        <a href="/export-users-csv" class="btn btn-success btn-sm">CSV</a>
                        <a href="/export-users-pdf" class="btn btn-success btn-sm">PDF</a>
                        <a href="/export-users-excel" class="btn btn-success btn-sm">Excel</a>
                    </div>
                </div>
            </div>
            <div class="p-3 d-block card-body">
                <div class="row">
                    <div class="col-md-12">
                       
                        <table id="usersTable" class="table table-bordered table-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="border-top-0">S.N</th>
                                    <th class="border-top-0">Name</th>
                                    <th class="border-top-0">Phone</th>
                                    <th class="border-top-0">Email</th>
                                    <th class="border-top-0">Address</th>
                                    <th class="border-top-0">City</th>
                                    <th class="border-top-0">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('js')

<script type="text/javascript">
    // import swal from 'sweetalert2';
    $(document).ready(function() {
      ;
        var currentadminId= '{{Auth::id()}}';
        //console.log(currentadminId);
        var now = new Date();
        
        let t = $('#usersTable').DataTable({
            dom: 'Bfrtip',
            ajax: "/api/get-users/" + currentadminId,
            columns: [
                { data: null },
                { data: null, render: function ( data, type, row ) {
                    let fname = data.fname;
                    let mname = data.mname ? data.mname : ' ';
                    let lname = data.lname ? data.lname : ' ';
                    let verified_icon = '<svg xmlns="http://www.w3.org/2000/svg" class="text-right" fill="#1760ff" height="15px" viewBox="0 0 24 24" width="15px"><g><rect fill="none" height="24" width="24"/></g><g><path d="M23,12l-2.44-2.79l0.34-3.69l-3.61-0.82L15.4,1.5L12,2.96L8.6,1.5L6.71,4.69L3.1,5.5L3.44,9.2L1,12l2.44,2.79l-0.34,3.7 l3.61,0.82L8.6,22.5l3.4-1.47l3.4,1.46l1.89-3.19l3.61-0.82l-0.34-3.69L23,12z M10.09,16.72l-3.8-3.81l1.48-1.48l2.32,2.33 l5.85-5.87l1.48,1.48L10.09,16.72z"/></g></svg>';
                    let name = fname +' '+ mname + ' ' + lname;
                    let icon = data.is_verified == 1 ? verified_icon: "";
                    console.log(icon, data.is_verified);
                    return '<div class="d-flex justify-content-between"><span>'+name+'</span>'+ icon +'</div>';
                } },
                { data: "phone" },
                { data: "email" },
                { data: null, render: function ( data, type, row ) {
                    let address = data.residence_profile ? data.residence_profile.address : '-' ;
                    return address;
                }},
                { data: null, render: function ( data, type, row ) {
                    let city = data.residence_profile ? data.residence_profile.city : '-' ;
                    return city;
                }},
                { data: 'email',render:function(data, type, row){
                    return '<div class="text-center dropdown"> <button class="btn btn-success" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i> </button>  <div class="dropdown-menu"> <a href="view_family/'+row.id+'" class="dropdown-item" title="Edit"><i class="fa fa-users text-info"></i>&nbsp; View Family</a><a href="fetch_users/'+row.id+'" class="dropdown-item"><i class="fa fa-eye text-info"></i>&nbsp; Edit User</a><a href="remove_user/'+row.id+'" onclick="return confirmDelete()" user-id="'+row.id+'" class="dropdown-item text-danger"><i class="fa fa-trash-o"></i>&nbsp; Delete User</a>  </div>   </div>';
                }},
            ],
            select: true,
            colReorder: true,
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
    } ).draw();
    });
    // Main Delete Confirmation 
    function confirmDelete() {
        if (confirm("Delete Record?") == true) {
            alert("Now deleting");
            return true;
        } else {
            alert("Cancelled by user");
            return false;
        }
    }
    // User deletion
    function removeUser(data){
        let user_id = $(data).attr('user-id');
        
        Swal.fire({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this User record!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (e) {
            if (e) {
                $.ajax({
                    type: 'GET',
                    url: "/dashboard/remove_user/" + user_id,
                    dataType: 'JSON',
                    success: function (results) {
                        let delete_user = results.user_record;
                        if (delete_user) {
                            Swal.fire("Success! User has been deleted!", {
                                icon: "success",
                            });
                        } else {
                            Swal.fire("Your Is not found!");
                        }
                    }
                });
            } else {
                Swal.fire("User is safe!");
            }

        }, function (dismiss) {
            return false;
        })
    }
   </script>






@endpush
