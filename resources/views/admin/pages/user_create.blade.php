@extends('admin.layout.app')

@section('title', 'Create User')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-user icon-gradient bg-mean-fruit"></i>
                </div>
                <div>Create User
                    <div class="page-title-subheading">Create user from here.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="tabs-animation">
        <div class="mb-3 card">
            <div class="card-header-tab card-header">
                <div class="col-md-2">
                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                        <i class="header-icon lnr-charts icon-gradient bg-happy-green"></i><h5>Create User</h5> 
                    </div>
                </div>
            </div>
            <div class="error-field">
                @if (session('status'))
                    <div class="mb-4 text-sm font-medium text-success">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
            <div class="p-3 d-block card-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ Route::is('users.edit') ? Route('users.update', $useredit->id) : Route('users.index') }}"
                            method="POST" enctype="multipart/form-data" class="">
                            @csrf
                            @if (Route::is('users.edit'))
                                @method('PUT')
                            @endif
                            {{-- @if ($errors->any()) border-bottom
                            <div class="alert alert-danger">
                                <ul>form-horizontal form-material
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif   --}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">First Name</label>
                                        <div class="col-md-12">
                                            <input type="text" name="fname" 
                                                value="{{ Route::is('users.edit') ? $useredit->fname : old('fname') }}"
                                                placeholder="First Name" class="form-control">
                                        </div>
                                        @error('fname')
                                            <span class="mt-1 text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Middle Name</label>
                                        <div class="col-md-12 ">
                                            <input type="text" name="mname" class="form-control"
                                                value="{{ Route::is('users.edit') ? $useredit->mname : old('mname') }}"
                                                placeholder="Middle Name" class="form-control">
                                        </div>
                                        @error('mname')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Last Name</label>
                                        <div class="col-md-12">
                                            <input type="text" name="lname" class="form-control"
                                                value="{{ Route::is('users.edit') ? $useredit->lname : old('lname') }}"
                                                placeholder="Last Name" class="p-0 border-0 form-control">
                                        </div>
                                        @error('lname')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Phone Number</label>
                                        <div class="col-md-12">
                                            <input type="text" onkeypress="return /^-?\d*$/.test(event.key)" name="phone"
                                            value="{{ Route::is('users.edit') ? $useredit->phone : old('phone') }}"
                                                maxlength="10" placeholder="XXXXXXXXXX" class="form-control">
                                        </div>
                                        @error('phone')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Upload Image</label>
                                        <div class="col-md-12">
                                            <input type="file" name="image" class="form-control">
                                        </div>
                                        @error('image')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Email</label>
                                        <div class="col-md-12">
                                            <input type="email" name="email"
                                                value="{{ Route::is('users.edit') ? $useredit->email : old('email') }}"
                                                placeholder="example@gmail.com" class="form-control">
                                        </div>
                                        @error('email')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Father Name</label>
                                        <div class="col-md-12">
                                            <input type="text" name="father_name"
                                                value="{{ Route::is('users.edit') ? $useredit->father_name : old('father_name') }}"
                                                placeholder="Father Name" class="form-control">
                                        </div>
                                        @error('father_name')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Father Number</label>
                                        <div class="col-md-12">
                                            <input type="text" onkeypress="return /^-?\d*$/.test(event.key)" name="fathernumber"
                                            value="{{ Route::is('users.edit') ? $useredit->fathernumber : old('fathernumber') }}"
                                                maxlength="10" placeholder="XXXXXXXXXX" class="form-control">
                                        </div>
                                        @error('fathernumber')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Select Gender</label>
                                        <div class="col-md-12">
                                            <select name="gender" id="" class="form-control">
                                                <option
                                                    value="{{ Route::is('users.edit') ? $useredit->gender : old('gender') }}">
                                                    {{ Route::is('users.edit') ? $useredit->gender : '-- select value--' }}
                                                </option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select>
                                        </div>
                                        @error('gender')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Date Of Birth</label>
                                        <div class="col-md-12">
                                            <input name="dob" 
                                                value="{{ Route::is('users.edit') ? $useredit->dob : old('dob') }}"
                                                placeholder="Selected date D.O.B" type="date"
                                                class="datepicker form-control">
                                        </div>
                                        @error('dob')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="bg-white page-breadcrumb">
                                        {{-- <h4 class="page-title">Residence Profile</h4> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Residence Address</label>
                                        <div class="col-md-12">
                                            <input type="text" name="address" class="form-control"
                                                value="{{ Route::is('users.edit') ? $useredit->ResidenceProfile->area : old('address') }}"
                                                placeholder="Address" class="p-0 border-0 form-control">
                                        </div>
                                        @error('address')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Residence Area</label>
                                        <div class="col-md-12">
                                            <input type="text" name="area" class="form-control"
                                                value="{{ Route::is('users.edit') ? $useredit->ResidenceProfile->area : old('area') }}"
                                                placeholder="Area" class="p-0 border-0 form-control">
                                        </div>
                                        @error('area')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Residence City</label>
                                        <div class="col-md-12">
                                            <input type="text" name="city" class="form-control"
                                                value="{{ Route::is('users.edit') ? $useredit->ResidenceProfile->city : old('city') }}"
                                                placeholder="City" class="p-0 border-0 form-control">
                                        </div>
                                        @error('city')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Residence Pincode</label>
                                        <div class="col-md-12">
                                            <input type="text" name="pincode" maxlength="6" minlength = "6"
                                                value="{{ Route::is('users.edit') ? $useredit->ResidenceProfile->pincode : old('pincode') }}"
                                                placeholder="Pincode" class="form-control">
                                        </div>
                                        @error('pincode')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Residence Phone</label>
                                        <div class="col-md-12">
                                            <input type="text" onkeypress="return /^-?\d*$/.test(event.key)" maxlength="10" name="primary_phone"
                                                value="{{ Route::is('users.edit') ? $useredit->ResidenceProfile->primary_phone : old('primary_phone') }}"
                                                placeholder="Primary Phone" class="form-control">
                                        </div>
                                        @error('primary_phone')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Residence Country</label>
                                        <div class="col-md-12">
                                            <input type="text" name="country"
                                                value="{{ Route::is('users.edit') ? $useredit->ResidenceProfile->country : old('country') }}"
                                                placeholder="Country" class="form-control">
                                        </div>
                                        @error('country')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Education</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" placeholder="Education">
                                        </div>
                                        @error('education')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="mb-4 form-group form-check">
                                        {{-- <label class="p-0 col-md-12">Job</label> --}}
                                        <div class="mt-4 col-md-12">
                                            <input type="checkbox" id="flexCheckChecked" name="job" class="form-check-input form-control" style="width:20px">
                                            <label class="mt-2 ml-3 form-check-label" for="flexCheckChecked" style="font-size: 18px">Job </label>
                                        </div>
                                        @error('job')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="mb-4 form-group form-check">
                                        {{-- <label class="p-0 col-md-12">Job</label> --}}
                                        <div class="mt-4 col-md-12">
                                            <input type="checkbox" name="matrimonial" id="flexCheckChecked1" class="mt-2 form-check-input form-control " style="width:20px" checked>
                                            <label class="mt-3 ml-3 form-check-label" for="flexCheckChecked1" style="font-size: 15px">Matrimonial</label>
                                        </div>
                                        @error('matrimonial')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Job Field</label>
                                        <div class="col-md-12">
                                            <input type="text" name="jobfield" value="{{old('jobfield')}}" class="form-control job" placeholder="Job" disabled>
                                        </div>
                                        @error('Job Field')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Job Experience</label>
                                        <div class="col-md-12">
                                            <input type="text" name="jobexperience" value="{{old('jobexperience')}}" class="form-control job" placeholder="Job Experience" disabled>
                                        </div>
                                        @error('Experience')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Job Location</label>
                                        <div class="col-md-12">
                                            <input type="text" name="joblocation" value="{{old('joblocation')}}" class="form-control job" placeholder="Current Job Location" disabled>
                                        </div>
                                        @error('Job Location')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Job CTC</label>
                                        <div class="col-md-12">
                                            <input type="text" name="jobctc" value="{{old('jobctc')}}" class="form-control job" placeholder="Current Job CTC" disabled>
                                        </div>
                                        @error('CTC')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                {{-- <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Date Of Anniversary</label>
                                        <div class="col-md-12 border-bottom">
                                            <input placeholder="Selected date anniversary" name="date_of_anniversary"
                                                value="{{ Route::is('users.edit') ? $useredit->date_of_anniversary : old('date_of_anniversary') }}"
                                                type="text" class="datepicker form-control">
                                        </div>
                                        @error('date_of_anniversary')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div> --}}
                                
                            </div>

                            <div class="row">
                               
                                
                                
                                
                                
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="bg-white page-breadcrumb card-header">
                                        <h5 class="page-title">Company Profile</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-4 row">
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Company Name</label>
                                        <div class="col-md-12">
                                            <input type="text" name="company_name"
                                                value="{{ Route::is('users.edit') ? $useredit->CompanyProfile->company_name : old('company_name') }}"
                                                placeholder="Name" class="form-control">
                                        </div>
                                        @error('company_name')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Company Address</label>
                                        <div class="col-md-12">
                                            <input type="text" name="company_address"
                                                value="{{ Route::is('users.edit') ? $useredit->CompanyProfile->company_address : old('company_address') }}"
                                                placeholder="Address" class="form-control">
                                        </div>
                                        @error('company_address')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Office Area</label>
                                        <div class="col-md-12">
                                            <input type="text" name="office_area"
                                                value="{{ Route::is('users.edit') ? $useredit->CompanyProfile->office_area : old('office_area') }}"
                                                placeholder="Area" class="form-control">
                                        </div>
                                        @error('office_area')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Company City</label>
                                        <div class="col-md-12">
                                            <input type="text" name="company_city"
                                                value="{{ Route::is('users.edit') ? $useredit->CompanyProfile->company_city : old('company_city') }}"
                                                placeholder="City" class="form-control">
                                        </div>
                                        @error('company_city')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Company State</label>
                                        <div class="col-md-12">
                                            <input type="text" name="company_state"
                                                value="{{ Route::is('users.edit') ? $useredit->CompanyProfile->company_state : old('company_state') }}"
                                                placeholder="State" class="form-control">
                                        </div>
                                        @error('company_state')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Company Country</label>
                                        <div class="col-md-12">
                                            <input type="text" name="company_country"
                                                value="{{ Route::is('users.edit') ? $useredit->CompanyProfile->company_country : old('company_country') }}"
                                                placeholder="Country" class="form-control">
                                        </div>
                                        @error('company_country')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Office Phone</label>
                                        <div class="col-md-12">
                                            <input type="text" onkeypress="return /^-?\d*$/.test(event.key)" maxlength="10" name="office_phone"
                                                value="{{ Route::is('users.edit') ? $useredit->CompanyProfile->office_phone : old('office_phone') }}"
                                                placeholder="XXXXXXXXXX" class="form-control">
                                        </div>
                                        @error('office_phone')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Company Email</label>
                                        <div class="col-md-12">
                                            <input type="email" name="company_email"
                                                value="{{ Route::is('users.edit') ? $useredit->CompanyProfile->company_email : old('company_email') }}"
                                                placeholder="Email" class="form-control">
                                        </div>
                                        @error('company_email')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Company Website</label>
                                        <div class="col-md-12">
                                            <input type="text" name="company_website"
                                                value="{{ Route::is('users.edit') ? $useredit->CompanyProfile->company_website : old('company_website') }}"
                                                placeholder="Website Link" class="form-control">
                                        </div>
                                        @error('company_website')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Sub Categories</label>
                                        <div class="col-md-12">
                                            <select name="subcategory_id" id="subcategory"  class="form-control">
                                                <option value="0">-- select subcategories -- </option>

                                                @foreach ($subcategories as $subcategory)
                                                    <option value="{{ $subcategory->id }}" id="subcategories">
                                                        {{ $subcategory->subcategory_name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('subcategory_id')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Categories</label>
                                        <div class="col-md-12">
                                            <select name="category_id" id="category" class="form-control">
                                                {{-- <option value="0">-- select categories --</option>
                                                @foreach ($categories as $category) {{ Route::is('users.edit') ? ($useredit->subcategory === $subcategory->id ? 'selected' : '') : old('subcategory_id') }}
                                                    <option value="{{ $category->id }}" id="categories"
                                                        {{ Route::is('users.edit') ? ($useredit->category === $category->id ? 'selected' : '') : old('category_id') }}>
                                                        {{ $category->categories_name }}</option>
                                                        {{ Route::is('users.edit') ? $useredit->Native->native_city : old('native_address') }}
                                                @endforeach --}}
                                            </select>
                                        </div>
                                        @error('category_id')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Business <a href="/dashboard/business/create" target="_blank">(or create new)</a> </label>
                                        <div class="col-md-12">
                                            <select name="business_id" id="business" class="form-control">
                                                {{-- <option value="0">-- select Business --</option>
                                                @foreach ($businesses as $business)
                                                    <option value="{{ $business->id }}" id="business"
                                                        {{ Route::is('users.edit') ? ($useredit->business === $business->id ? 'selected' : '') : old('business_id') }}>
                                                        {{ $business->title }}</option>
                                                @endforeach --}}
                                            </select>
                                        </div>
                                        @error('business_id')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="bg-white page-breadcrumb card-header">
                                        <h5 class="page-title">Native Profile</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-4 row">
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Native Address</label>
                                        <div class="col-md-12">
                                            <input type="text" name="native_address" value="{{ Route::is('users.edit') ? $useredit->Native->native_address : old('native_address') }}" placeholder="Native address"
                                                class="form-control">
                                        </div>
                                        @error('native_address')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Native City</label>
                                        <div class="col-md-12">
                                            <input type="text" name="native_city"  placeholder="Native City"
                                                class="form-control">
                                        </div>
                                        @error('native_city')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Native pincode</label>
                                        <div class="col-md-12">
                                            <input type="text" onkeypress="return /^-?\d*$/.test(event.key)" name="native_pincode" maxlength="6" value="{{ Route::is('users.edit') ? $useredit->Native->native_pincode : old('native_pincode') }}" placeholder="XXXXXX"
                                                class="form-control">
                                        </div>
                                        @error('native_pincode')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4 form-group">
                                        <label class="p-0 col-md-12">Is Committee member </label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label class="p-0 mt-1 col-md-12">yes </label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="checkbox" name="committe_member"  checked style="width:20px"
                                                            value="{{ 'checked' ? 1 : 0 }}" class="form-control ">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @error('committe_member')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="mb-4 form-group">
                                <div class="col-sm-12 text-center">
                                    <button type="submit"
                                        class="btn btn-success">{{ Route::is('fecth_users.edit') ? 'Update' : 'Submit' }}</button>
                                    <a class="btn btn-danger" href="/">Cancel</a>    
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    
        
        $('#flexCheckChecked').mousedown(function(){
            
            
            $(this).on('change',function(){
                var check = $(this).is(":checked");
                console.log(check);
                if(check==true){
                
                $('.job').prop('disabled',false)
            }else{
                $('.job').prop('disabled',true);
            }
            })
            
            
        });
    
    
</script>
<script>
    $(document).ready(function(){
        $('#subcategory').on('change',function(){
            var subCategoryId = $(this).val()
            console.log(subCategoryId);
            if(subCategoryId){
                $.ajax({
                    url : '/dashboard/fetch-category-for-subcategory/'+ subCategoryId,
                    type: "GET",
                    data : {"_token":"{{ csrf_token() }}"},
                    dataType: "JSON",
                    success:function(data){
                        if(data){
                            console.log(data);
                            $('#category').empty();
                            $('#category').focus;
                            $('#category').append('<option value="">-- Select Category --</option>');
                            $.each(data,function(key,value){
                               
                                    $('select[name="category_id"]').append('<option value="'+ value.id +'">' + value.categories_name+ '</option>');
                                
                                

                            });
                        }else{
                            $('#category').empty();
                           
                        }
                    }

                });
            }else{
                $('#category').empty();
            }
        }); 
    });
</script>
<script>
    $(document).ready(function(){
        $('#category').on('change',function(){
            var categoryId = $(this).val()
            console.log(categoryId);
            if(categoryId){
                $.ajax({
                    url : '/dashboard/fetch-business-for-category/'+ categoryId,
                    type: "GET",
                    data : {"_token":"{{ csrf_token() }}"},
                    dataType: "JSON",
                    success:function(data){
                        if(data){
                            console.log(data);
                            $('#business').empty();
                            $('#business').focus;
                            $('#business').append('<option value="">-- Select Business --</option>');
                            $.each(data,function(key,value){
                               
                                    $('select[name="business_id"]').append('<option value="'+ value.id +'">' + value.title+ '</option>');
                                
                                

                            });
                        }else{
                            $('#business').empty();
                           
                        }
                    }

                });
            }else{
                $('#busienss').empty();
            }
        }); 
    });
</script>
@endpush