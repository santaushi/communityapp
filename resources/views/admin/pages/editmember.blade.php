@extends('admin.layout.app')

@section('title', 'Edit Member')

@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-user icon-gradient bg-mean-fruit"></i>
                </div>
                <div>Edit Member
                    <div class="page-title-subheading">Edit Member </div>
                </div>
            </div>
            <div class="col-md-9 text-right">
                <a class="btn btn-success" href="{{url()->previous()}}"><i class="pe-7s-back pe-lg"></i></a>
            </div>
        </div>
    </div>
    
    <div class="tabs-animation">
        <div class="mb-3 card">
            <div class="card-header-tab card-header">
                <div class="row">
                    <div class="col-md-6">
                        @if (session('status'))
                        <div class="mb-4 text-sm font-medium text-success">
                            {{ session('status') }}
                        </div>
                        @elseif(!session('status'))
                            <div class="card-header-title font-size-lg text-capitalize font-weight-normal"><i class="header-icon lnr-charts icon-gradient bg-happy-green"></i>Edit Member
                            </div> 
                        @endif
                    </div>
                    <div class="col-md-6">
                        {{-- <a href="{{url('/dashboard/view_family/'. $user_id)}}" class="btn btn-primary" style="color:white">View Family</a> --}}
                    </div>
                </div>
               
                <div>
                   
                </div>
            </div> 

            <div class="p-3 d-block card-body">
                <form action="/dashboard/member_update/{{$member->id}}" method="POST" enctype="multipart/form-data" class="form-horizontal form-material">
                        @csrf
                        @method('PUT')
                        {{-- @if (Route::is('/dashboard/member_edit'))
                        
                        @endif --}}
                        <div class="row">
                            <input type="hidden" name="member_id" value="{{ $member->id }}"  class="p-0 border-0 form-control">
                            
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12"> Name</label>
                                    <div class="col-md-12">
                                        <input type="text" name="member_name" value="{{$member->member_name}}" placeholder="Name" class="form-control">
                                    </div>
                                    @error('member_name')
                                    <span class="mt-1 text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            {{-- <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">phone</label>
                                    <div class="col-md-12 border-bottom">
                                        <input type="text" name="member_phone" maxlength="13" value="{{ Route::is('member_edit') ? $member_edit->member_phone : old('member_phone') }}" placeholder="1234567890" class="p-0 border-0 form-control">
                                    </div>
                                    @error('member_phone')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div> --}}
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">Address</label>
                                    <div class="col-md-12">
                                        <input type="text" name="member_address" value="{{$member->member_address}}" placeholder="Address" class="form-control">
                                    </div>
                                    @error('member_address')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">City</label>
                                    <div class="col-md-12 ">
                                        <input type="text" name="member_city" value="{{ $member->member_city }}" placeholder="City" class="form-control">
                                    </div>
                                    @error('member_city')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">Pin code</label>
                                    <div class="col-md-12">
                                        <input type="text" name="member_pincode" maxlength="6" value="{{$member->member_pincode}}" placeholder="Pin Code" class="form-control">
                                    </div>
                                    @error('member_pincode')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                           
                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">Image</label>
                                    <div class="col-md-12 ">
                                        <input type="file" name="member_image"  class="form-control">
                                    </div>
                                    @error('member_image')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="mb-4 form-group">
                                    <label class="p-0 col-md-12">Select Relation</label>
                                    <div class="col-md-12">
                                        <select name="member_relation" id="" class="form-control">
                                            <option value="{{$member->member_relation}}">{{$member->member_relation}}</option>
                                            <option value="brother">Brother</option>
                                            <option value="father">Father</option>
                                        </select>
                                    </div>
                                    @error('member_relation')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="mb-4 form-group">
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-success" name="action" value="update" class="register">Update</button>
                                <a type="submit" class="btn btn-danger" href="{{Route('view_family',$userId)}}">Cancel</a>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

@endsection
