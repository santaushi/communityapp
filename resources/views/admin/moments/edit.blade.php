@extends('admin.layout.app')
@section('title', 'Edit moment')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-note2 icon-gradient bg-mean-fruit"></i>
                </div>
                <div>Moments
                    <div class="page-title-subheading">Edit moment.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="tabs-animation">
        <div class="mb-3 card">
            <div class="card-header-tab card-header">
                <div class="col-md-2">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    <i class="header-icon lnr-charts icon-gradient bg-happy-green"> </i> Edit Moments
                </div>
                </div>
                {{-- <div class="text-right col-md-10">
                    <a href="/dashboard/moments/create" class="btn btn-success pull-right">Edit</a>
                </div> --}}
                {{-- <div class="text-right col-md-2">
                    <a href="/dashboard/moments/create" class="btn btn-success pull-right">Cancel</a>
                </div> --}}
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="p-3 d-block card-body">
                <form action="/dashboard/moments/{{$moments->id}}" method="POST" class="was-validated" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}
                    <input type="hidden" name="id" value="{{$moments->id}}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" id="title" name="title" class="form-control" value="{{$moments->title}}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <input type="text" id="description" name="description" class="form-control" value="{{$moments->description}}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <input type="file" name="image" id="image" class="dropify">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="text-center col-md-12">
                            <button type="submit" class="btn btn-success" name="action" value="update" class="register">Save</button>
                            <button type="submit" class="btn btn-success" name="action" value="cancel" class="register">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@push('js')
    <script>
        $('.dropify').dropify();
    </script>
@endpush
