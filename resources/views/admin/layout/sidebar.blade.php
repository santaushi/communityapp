<div class="scrollbar-sidebar">
    <div class="app-sidebar__inner">
        <ul class="vertical-nav-menu">
            <li class="app-sidebar__heading">Menu</li>
            <li class="">
                <a href="/dashboard" class="{{ Request::is('dashboard') ? 'mm-active' : '' }}">
                    <i class="metismenu-icon pe-7s-graph1"></i>Dashboard
                </a>
            </li>
            
            <li class="app-sidebar__heading" class="">Manage Users</li>
            <li class="{{ Request::is('dashboard/users') ? 'mm-active' : '' }} {{ Request::is('dashboard/create_user') ? 'mm-active' : '' }}">
                <a href="#" >
                    <i class="metismenu-icon pe-7s-user"></i>Users
                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                </a>
                <ul class="{{ Request::is('dashboard/users') ? 'mm-show' : '' }}">
                    <li>
                        <a href="/dashboard/users" class="{{ Request::is('dashboard/users') ? 'mm-active' : '' }}">
                            <i class="metismenu-icon "></i>User List
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/dashboard/create_user') }}" class="{{ Request::is('dashboard/create_user') ? 'mm-active' : '' }}">
                            <i class="metismenu-icon pe-7s-user"></i>
                            Create User
                        </a>
                    </li>
                </ul>
            </li>
            <li class="app-sidebar__heading" class="">Manage Categories</li>
            <li class="{{ Request::is('dashboard/categories') ? 'mm-active' : '' }} {{ Request::is('dashboard/subcategories') ? 'mm-active' : '' }}">
                <a href="#">
                    <i class="metismenu-icon pe-7s-display2"></i>Categories
                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                </a>
                <ul class="{{ Request::is('dashboard/categories') ? 'mm-show' : '' }}">
                    <li>
                        <a href="{{ url('/dashboard/categories') }}" class="{{ Request::is('dashboard/categories') ? 'mm-active' : '' }}" >
                            <i class="metismenu-icon"></i>Categories
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/dashboard/subcategories') }}" class="{{ Request::is('dashboard/subcategories') ? 'mm-active' : '' }}" >
                            <i class="metismenu-icon pe-7s-user"></i>
                            Sub-category
                        </a>
                    </li>
                </ul>
            </li>
            <li class="app-sidebar__heading" class="">Manage News</li>
            <li>
                <a href="{{ url('/dashboard/newsList') }}" class="{{ Request::is('dashboard/news') ? 'mm-active' : '' }} {{ Request::is('dashboard/newsList') ? 'mm-active' : '' }}">
                    <i class="metismenu-icon pe-7s-news-paper"></i>News
                </a>
            </li>
            <li class="app-sidebar__heading" class="">Manage Events</li>
            <li>
                <a href="{{ url('/dashboard/events') }}" class="{{ Request::is('dashboard/events') ? 'mm-active' : '' }}">
                    <i class="metismenu-icon pe-7s-date"></i>Events
                </a>
                
            </li>


            {{-- <li class="app-sidebar__heading" class="">Manage Moments</li>
            <li>
                <a href="{{ url('/dashboard/moments') }}" class="{{ Request::is('dashboard/moments') ? 'mm-active' : '' }}">
                    <i class="metismenu-icon pe-7s-news-paper"></i>Moments
                </a>
            </li> --}}

            <li class="app-sidebar__heading" class="">Manage Business</li>
            <li class="{{Request::is('/dashboard/business')? 'mm-active':''}}"> 
                <a href="{{Route('business.index')}}" class="{{ Request::is('dashboard/business') ? 'mm-active' : '' }}">
                    <i class="metismenu-icon pe-7s-portfolio" ></i>Business 
                    {{-- <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> --}}
                </a>
                {{-- <ul class="{{ Request::is('dashboard/business') ? 'mm-show' : '' }} {{ Request::is('dashboard/business/create') ? 'mm-show' : '' }}" >
                    <li>
                        <a  href="{{Route('business.index')}}" class="{{ Request::is('dashboard/business') ? 'mm-active' : '' }}" >
                            <i class="metismenu-icon"></i>Business list
                        </a>
                    </li>
                   
                </ul> --}}
            </li>   
            <li class="app-sidebar__heading" class="">Manage Committee</li>
            <li>
                <a href="{{ url('/dashboard/committee') }}" class="{{ Request::is('dashboard/committee') ? 'mm-active' : '' }}">
                    <i class="metismenu-icon pe-7s-albums"></i>Committee
                </a>
            </li> 
            
        </ul>
    </div>
</div>