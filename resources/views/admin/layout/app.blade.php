
<!doctype html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>@yield('title') </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <link rel="icon"  type="image/png" href="{{ asset('images/favicon.png') }}">
    <!-- Disable tap highlight on IE --> 
    <meta name="msapplication-tap-highlight" content="no">
    <link href="{{ asset('assets/admin/css/admin.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/dist/icons/Icon-font-7/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    
   
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    {{-- Dropify --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css">
    {{-- Summernote --}}
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.css" id="theme-styles">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
        ]) !!};
        
    </script>
    <style>
        #myUL {
  list-style-type: none;
  padding: 0;
  margin: 0;
}

#myUL li a {
  border: 1px solid #ddd;
  margin-top: -1px; /* Prevent double borders */
  background-color: #f6f6f6;
  padding: 12px;
  text-decoration: none;
  font-size: 18px;
  color: black;
  display: block
}

#myUL li a:hover:not(.header) {
  background-color: #eee;
}
    </style>
</head>
<body>
    @if (Auth::user())
        <div class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar" id="app">
            @include('admin.layout.navbar')  
            <div class="app-main">
                <div class="app-sidebar sidebar-shadow bg-royal sidebar-text-light ">
                    @include('admin.layout.sidebar')
                </div>
                <div class="app-main__outer">
                    @yield('content')
                </div>
            </div>
        </div>
    @else   
        <div class="app-main__outer">
            @yield('content')
        </div>
    @endif
    <div class="app-drawer-overlay d-none animated fadeIn"></div>
    <script src="{{ asset('assets/admin/js/admin.js') }}" defer></script>
    <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
    {{-- <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script> --}}
    {{-- <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script> --}}
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    {{-- <script src="{{ asset('assets/admin/js/bootstrap-confirmation.js') }}" defer></script> --}}
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
    <script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    {{-- SummerNote --}}
    
    <script src="{{ asset('assets/plugins/Dropify/dropify.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    
    {{-- Fuse js --}}
    <script src="https://cdn.jsdelivr.net/npm/fuse.js/dist/fuse.js"></script>
    
    @stack('js')
    <script>
        $(document).ready(function(){
          $('#zooming').on('click',function(){
            $('.app-sidebar').toggle();

            // $('.app-header').toggle();
            
          })  
            
        })
    </script>

    <script>
        function searchItems(){
            let searchTerm = $("#dropdownMenuButton").val();
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                });
                $.ajax({
                    type:'POST',
                    url:"/api/fetch-all-items-for-search",
                    dataType:'json',//return data will be json
                    success:function(data){
                        let list = data.items;
                        // console.log(list)
                        const options = {
                            includeScore: true,
                            // Search in `author` and in `tags` array
                            keys: ['member_name','fname','mname','lname']
                        }

                        const fuse = new Fuse(list, options)
                        const result = fuse.search(searchTerm);
                        $("#search_results").empty();
                        result.map( search_item => {
                            $("#search_results").show();
                            let str = '';
                            if(search_item.item.item_type == 'user'){
                                let item = search_item.item;
                                let user = '<a href="/dashboard/user/'+item.id+'" target="_blank" class="list-group-item list-group-item-action">'+item.fname+'</a>'
                                $("#search_results").append(user);
                            }
                            if(search_item.item.item_type == 'member'){
                                let member_item = search_item.item;
                                let member = '<a href="/dashboard/user/'+member_item.id+'" target="_blank" class="list-group-item list-group-item-action">'+member_item.member_name+'</a>'
                                $("#search_results").append(member);
                            }
                            
                        })

                        }
                });

        }
    </script>
    <script>
        var goFS = document.getElementById("goFS");
        goFS.addEventListener("click", function() {
            document.body.requestFullscreen();
          
        }, false);
      </script>
</body>

</html>