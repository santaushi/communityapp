<div class="app-header header-shadow bg-royal header-text-light">
    <div class="app-header__logo" >
        <div class="logo-src">
            {{-- <h4>Community</h4> --}}
            <img src="{{asset('images/logo.png')}}" alt="Bidasar" style="width: 200%;margin-top:-20px">
        </div>
        <div class="ml-auto header__pane">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>    
    <div class="app-header__content">
        <div class="app-header-left">
            <div class="search-wrapper">
                <div class="input-holder">
                    <input type="text" class="search-input" placeholder="Type to search" autocomplete="off" onkeyup="searchItems()" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <button class="search-icon"><span></span></button>
                </div>
                <button class="close"></button>
            </div>   
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="search_results" style="margin-left: 21%; width:270px">
            </div>
        </div>
        <div class="app-header-right" >
            <div class="header-dots">
                {{-- <div>
                    <button class="btn btn-default" id="zooming">
                           hide
                    </button>
                </div>  --}}
                <div class="dropdown" style="display:none">
                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown"
                        class="p-0 mr-2 btn btn-link">
                        <span class="icon-wrapper icon-wrapper-alt rounded-circle">
                            <span class="icon-wrapper-bg bg-danger"></span>
                            {{-- <i class="icon text-danger icon-anim-pulse ion-android-notifications"></i> --}}
                            <span class="material-icons icon-center">notification_important</span>
                            <span class="badge badge-dot badge-dot-sm badge-danger">Notifications</span>
                        </span>
                    </button>
                    <div tabindex="-1" role="menu" aria-hidden="true" 
                        class="dropdown-menu-xl rm-pointers dropdown-menu dropdown-menu-right">
                        <div class="mb-0 dropdown-menu-header">
                            <div class="dropdown-menu-header-inner bg-deep-blue">
                                <div class="menu-header-image opacity-1" style="background-image: url('assets/images/dropdown-header/city3.jpg');"></div>
                                <div class="menu-header-content text-dark">
                                    <h5 class="menu-header-title">Notifications</h5>
                                    <h6 class="menu-header-subtitle">You have <b>21</b> unread messages</h6>
                                </div>
                            </div>
                        </div>
                        <ul class="p-3 tabs-animated-shadow tabs-animated nav nav-justified tabs-shadow-bordered">
                            <li class="nav-item">
                                <a role="tab" class="nav-link active" data-toggle="tab" href="#tab-messages-header">
                                    <span>Messages</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a role="tab" class="nav-link" data-toggle="tab" href="#tab-events-header">
                                    <span>Events</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a role="tab" class="nav-link" data-toggle="tab" href="#tab-errors-header">
                                    <span>System Errors</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
            <i class="text-white pe-7s-expand1" id="goFS" style="cursor: pointer"></i>
            
            <div class="pr-0 header-btn-lg">
                <div class="p-0 widget-content">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="btn-group">
                                <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                
                                    @if(Auth::user()->image)
                                        <img width="42" class="rounded-circle" src="{{ asset('users/images/'.Auth::user()->image)}}" alt="">
                                    @else 
                                        <img width="42" class="rounded-circle" src="{{asset('users/images/default-user.png')}}" alt="">
                                    @endif
                                    <i class="ml-2 fa fa-angle-down opacity-8"></i>
                                </a>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right">
                                    <div class="dropdown-menu-header">
                                        <div class="dropdown-menu-header-inner bg-info">
                                            <div class="menu-header-image opacity-2" style="background-image: url('assets/images/dropdown-header/city3.jpg');"></div>
                                            <div class="text-left menu-header-content">
                                                <div class="p-0 widget-content">
                                                    {{Auth::user()->fname}} {{Auth::user()->mname}} {{Auth::user()->lname}}
                                                    <div class="widget-content-wrapper">
                                                        <div class="mr-3 widget-content-left">
                                                            <img width="42" class="rounded-circle" src="assets/images/avatars/1.jpg" alt="">
                                                        </div>
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">@if(Auth::user()) {{Auth::user()->name}} @endif</div>
                                                            <div class="widget-subheading opacity-8">{{Auth::user()->bio}} </div>
                                                        </div>
                                                        <div class="mr-2 widget-content-right">
                                                            {{-- class="btn-pill btn-shadow btn-shine btn btn-focus" --}}
                                                            <a type="submit" class="btn-wide btn btn-primary  btn-md" href="/dashboard/admin/profile">Edit Profile</a>
                                                            {{-- <button class="btn-pill btn-shadow btn-shine btn btn-focus" 
                                                                href="{{ route('logout') }}"
                                                                onclick="event.preventDefault();
                                                                                document.getElementById('logout-form').submit();"
                                                            >{{ __('Logout') }} <i class="metismenu-icon pe-7s-power" ></i>
                                                            </button> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="scroll-area-xs" style="height: 150px;">
                                        <div class="scrollbar-container ps">
                                            <ul class="nav flex-column">
                                                <li class="nav-item-header nav-item">Activity</li>
                                                {{-- <li class="nav-item">
                                                    <a href="javascript:void(0);" class="nav-link">Chat
                                                        <div class="ml-auto badge badge-pill badge-info">8</div>
                                                    </a>
                                                </li> --}}
                                                {{-- <li class="nav-item">
                                                    <a href="javascript:void(0);" class="nav-link">Recover Password</a>
                                                </li> --}}
                                                {{-- <li class="nav-item-header nav-item">My Account
                                                </li>
                                                <li class="nav-item">
                                                    <a href="javascript:void(0);" class="nav-link">Settings
                                                        <div class="ml-auto badge badge-success">New</div>
                                                    </a>
                                                </li> --}}
                                                {{-- <li class="nav-item">
                                                    <a href="javascript:void(0);" class="nav-link">Messages
                                                        <div class="ml-auto badge badge-warning">512</div>
                                                    </a>
                                                </li> --}}
                                                {{-- <li class="nav-item">
                                                    <a href="javascript:void(0);" class="nav-link">Logs</a>
                                                </li> --}}
                                            </ul>
                                        </div>
                                    </div>
                                    <ul class="nav flex-column">
                                        <li class="mb-0 nav-item-divider nav-item"></li>
                                    </ul>
                                    <div class="grid-menu grid-menu-2col">
                                        <div class="no-gutters row">
                                            {{-- <div class="col-sm-6">
                                                <button class="pt-2 pb-2 btn-icon-vertical btn-transition btn-transition-alt btn btn-outline-warning">
                                                    <i class="mb-2 pe-7s-chat icon-gradient bg-amy-crisp btn-icon-wrapper"></i> Message Inbox
                                                </button>
                                            </div> --}}
                                            <div class="col-sm-6">
                                                {{-- <button class="pt-2 pb-2 btn-icon-vertical btn-transition btn-transition-alt btn btn-outline-danger">
                                                    <i class="mb-2 pe-7s-ticket icon-gradient bg-love-kiss btn-icon-wrapper"></i>
                                                    <b>Support Tickets</b>
                                                </button> --}}
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="nav flex-column">
                                        <li class="nav-item-divider nav-item">
                                        </li>
                                        <li class="text-center nav-item-btn nav-item">
                                            <button class="btn-pill btn-shadow btn-shine btn btn-focus" 
                                            href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();"
                                        >{{ __('Logout') }} <i class="metismenu-icon pe-7s-power" ></i>
                                        </button>
                                            
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="ml-3 widget-content-left header-user-info">
                            <div class="widget-heading"> @if(Auth::user()) {{Auth::user()->name}} @endif </div>
                            <div class="widget-subheading"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
   