@extends('admin.layout.app')
@section('title', 'Committee')

@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-albums icon-gradient bg-mean-fruit"></i>
                </div>
                <div>Committee
                </div>
            </div>
        </div>
    </div>

    <div class="tabs-animation">
        <div class="mb-3 card">
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session()->get('success') }}
                </div>
            @endif
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissible fade show">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            @if(count($committee) <= 0)
            <div class="card-header-tab card-header">
                <div class="col-md-2">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    <i class="header-icon lnr-charts icon-gradient bg-happy-green"> </i>Add Committee
                </div>
                </div>
            </div>
            <div class="p-3 d-block card-body">
                <form action="/dashboard/committee" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <input type="file" name="image" class="dropify">
                    </div>
                </div>
                <div class="mt-2 text-center row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success"><i class="pe-7s-cloud-upload"></i>Save</button>
                        <a class="btn btn-danger" href="/dashboard/committee">Cancel</a>
                    </div>
                </div>
            </form>
            </div>
            @endif
        </div>
    </div>
    <div class="tabs-animation">
        <div class="mb-3 card">
            <div class="card-header-tab card-header">
                <div class="col-md-2">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    <i class="header-icon lnr-charts icon-gradient bg-happy-green"> </i> Committee
                </div>
                </div>
            </div>
            <div class="p-3 d-block card-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="momentsTable" class="table table-bordered table-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="text-center border-top-0" width="5%">S.N</th>
                                    <th class="text-center border-top-0" width="10%">Image</th>
                                    <th class="text-center border-top-0" width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($committee as $comm)
                                    <tr>
                                        <td class="text-center">{{$loop->index+1}} </td>
                                        <td class="text-center">
                                            <img src="{{asset($comm->image)}}" class="img-thumbnail img-responsive">
                                        </td>
                                        <td class="text-center dropdown">
                                            <button class="btn btn-success" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i> </button> 
                                            <div class="text-center dropdown-menu" style="min-width:75px">
                                                <form action="{{ route('committee.destroy', $comm->id) }}" method="POST">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button class="btn btn-danger"> <i class="fa fa-trash-o"></i> Delete</button>
                                                </form>
                                            </div>    
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('js')
<script>
    $('.dropify').dropify();
</script>
@endpush
