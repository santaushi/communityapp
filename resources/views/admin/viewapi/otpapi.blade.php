@extends('admin.layout.app')
@section('title', 'Member List')
@section('content')
<div class="row">
    <form id="myotpform">
        <div class="col-md-10 ml-4 mt-2">
            <input type="tel" id="phonenum" name="phone" pattern="[0-9]{10}"

            maxlength="10" placeholder="XXXXXXXXXX" class="form-control">
        </div>
        <div class="col-md-2 ml-4 mt-2">
            <button type="submit" class="btn btn-success">send otp</button>
        </div>
    </form>
</div>
<div class="row">
    <form id="otpverificationform">
        
        <div class="col-md-10 ml-5 mt-3">
            <h6>verification:</h6>
            <label for="phone">Phone</label>
            <input type="tel" id="phonenum" name="phone" pattern="[0-9]{10}"

            maxlength="10" placeholder="XXXXXXXXXX" class="form-control">
            <input type="text" name="otp" placeholder="6 digit otp">
        </div>
        <div class="col-md-2 ml-5 mt-3">
            <button type="submit" class="btn btn-success">verify</button>
        </div>
    </form>
</div>
<div class="row">
    <form id="searchform">
        
        <div class="col-md-10 ml-5 mt-3">
            <h6>search:</h6>
            {{-- <label for="phone">Phone</label> --}}
            <input type="search" name="search" class="form-control rounded" placeholder="Search" aria-label="Search"
            aria-describedby="search-addon" />
        </div>
        {{-- <div class="col-md-2 ml-5 mt-3">
            <button type="submit" class="btn btn-success">verify</button>
        </div> --}}
    </form>
</div>
<div class="row">
    <button id="Job" class="btn btn-success ml-5 mt-3">Job</button>
</div>

@endsection
@push('js')
    <script>
        $(document).ready(function(){
            $("#myotpform").on('submit',function (event) {
            event.preventDefault();
             
            $.ajax({
               
            
            url: "/api/login",
            method: "POST",
            data: new FormData(this),
            dataType:  "JSON",
            contentType: false,
            cache: false,
            processData: false,
            }).done(function (data) {
                console.log(data);
            });
        }); 
        $("#otpverificationform").on('submit',function (event) {
            event.preventDefault();
             
            $.ajax({
               
            
            url: "/api/otpVerified",
            method: "POST",
            data: new FormData(this),
            dataType:  "JSON",
            contentType: false,
            cache: false,
            processData: false,
            }).done(function (data) {
                console.log(data);
            });
        }); 
        $("#searchform").on('submit',function (event) {
            event.preventDefault();
             
            $.ajax({
               
            
            url: "/api/search",
            method: "POST",
            data: new FormData(this),
            dataType:  "JSON",
            contentType: false,
            cache: false,
            processData: false,
            }).done(function (data) {
                console.log(data);
            });
        });

        $('#job').on('click',function(){
            event.preventDefault();
            $.ajax({
               
            
               url: "/api/job",
               method: "GET",
               data: new FormData(this),
               dataType:  "JSON",
               contentType: false,
               cache: false,
               processData: false,
               }).done(function (data) {
                   console.log(data);
               });
        }); 
        })
         
    </script>
@endpush