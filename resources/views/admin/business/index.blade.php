@extends('admin.layout.app')
@section('title', 'Business')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-portfolio icon-gradient bg-mean-fruit"></i>
                </div>
                <div>Business
                    <div class="page-title-subheading">All Business list.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="tabs-animation">
        <div class="mb-3 card">
            <div class="card-header-tab card-header">
                <div class="col-md-2">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    <i class="header-icon lnr-charts icon-gradient bg-happy-green"> </i> All Businesses
                </div>
                </div>
                <div class="text-right col-md-10">
                    <a href="/dashboard/business/create" class="btn btn-success pull-right">Add Business</a>
                </div>
            </div>
            <div class="p-3 d-block card-body">
                <div class="row">
                    <div class="col-md-12">
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissible fade show">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ session()->get('success') }}
                            </div>
                        @endif
                        <table id="businessTable" class="table table-bordered table-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="text-center border-top-0" width="5%">S.N</th>
                                    <th class="text-center border-top-0" width="10%">Title</th>
                                    <th class="text-center border-top-0" >Description</th>
                                    <th class="text-center border-top-0" width="10%">Category</th>
                                    <th class="text-center border-top-0" width="10%">Sub Category</th>
                                    <th class="text-center border-top-0" width="10%">Image</th>
                                    <th class="text-center border-top-0" width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($BusinessCategory as $businessCategory)
                                    <tr>
                                        <td class="text-center">{{$loop->iteration}} </td>
                                        <td class="text-center">
                                            {{$businessCategory->title}}
                                        </td>
                                        <td class="text-center">
                                            {{$businessCategory->description}}
                                        </td>
                                        <td class="text-center">
                                            {{$businessCategory->category->categories_name}}
                                        </td>
                                        <td class="text-center">
                                            {{$businessCategory->subcategory->subcategory_name}}
                                        </td>

                                        <td class="text-center">
                                            <img src='{{asset($businessCategory->image)}}' class="img-thumbnail img-responsive">
                                        </td>
                                        <td class="text-center dropdown">
                                            <button class="btn btn-success" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i> </button> 
                                            <div class="dropdown-menu text-center" style="min-width:75px">
                                                <a  class="dropdown-item" href="{{Route('business.edit',$businessCategory->id)}}" target="__self" title="Edit" style="cursor: pointer">
                                                <svg height="18" viewBox="0 0 512 511" width="18" xmlns="http://www.w3.org/2000/svg"><path d="m362.667969 512.484375h-298.667969c-35.285156 0-64-28.714844-64-64v-298.667969c0-35.285156 28.714844-64 64-64h170.667969c11.796875 0 21.332031 9.558594 21.332031 21.335938 0 11.773437-9.535156 21.332031-21.332031 21.332031h-170.667969c-11.777344 0-21.332031 9.578125-21.332031 21.332031v298.667969c0 11.753906 9.554687 21.332031 21.332031 21.332031h298.667969c11.773437 0 21.332031-9.578125 21.332031-21.332031v-170.667969c0-11.773437 9.535156-21.332031 21.332031-21.332031s21.335938 9.558594 21.335938 21.332031v170.667969c0 35.285156-28.714844 64-64 64zm0 0" fill="#607d8b"/><g fill="#42a5f5"><path d="m368.8125 68.261719-168.792969 168.789062c-1.492187 1.492188-2.496093 3.390625-2.921875 5.4375l-15.082031 75.4375c-.703125 3.496094.40625 7.101563 2.921875 9.640625 2.027344 2.027344 4.757812 3.113282 7.554688 3.113282.679687 0 1.386718-.0625 2.089843-.210938l75.414063-15.082031c2.089844-.429688 3.988281-1.429688 5.460937-2.925781l168.789063-168.789063zm0 0"/><path d="m496.382812 16.101562c-20.796874-20.800781-54.632812-20.800781-75.414062 0l-29.523438 29.523438 75.414063 75.414062 29.523437-29.527343c10.070313-10.046875 15.617188-23.445313 15.617188-37.695313s-5.546875-27.648437-15.617188-37.714844zm0 0"/></g></svg><span>&nbsp Edit</span></a></li>
                                                
                                                <a type="submit"  class="dropdown-item" title="Delete" style="cursor: pointer" href="{{"business/delete/".$businessCategory->id}}">
                                                <svg height="18" viewBox="0 0 512 512" width="18" xmlns="http://www.w3.org/2000/svg"><path d="m256 80h-32v-48h-64v48h-32v-80h128zm0 0" fill="#62808c"/><path d="m304 512h-224c-26.507812 0-48-21.492188-48-48v-336h320v336c0 26.507812-21.492188 48-48 48zm0 0" fill="#e76e54"/><path d="m384 160h-384v-64c0-17.671875 14.328125-32 32-32h320c17.671875 0 32 14.328125 32 32zm0 0" fill="#77959e"/><path d="m260 260c-6.246094-6.246094-16.375-6.246094-22.625 0l-41.375 41.375-41.375-41.375c-6.25-6.246094-16.378906-6.246094-22.625 0s-6.246094 16.375 0 22.625l41.375 41.375-41.375 41.375c-6.246094 6.25-6.246094 16.378906 0 22.625s16.375 6.246094 22.625 0l41.375-41.375 41.375 41.375c6.25 6.246094 16.378906 6.246094 22.625 0s6.246094-16.375 0-22.625l-41.375-41.375 41.375-41.375c6.246094-6.25 6.246094-16.378906 0-22.625zm0 0" fill="#fff"/></svg><span>&nbsp Delete</span></a></li>
                                            </div>    
                                        </td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

  
</div>
@endsection
@push('js')
    <script>

        $(document).ready( function () {
            $('#businessTable').DataTable();
        }); 
    </script>
@endpush