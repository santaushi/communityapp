@extends('admin.layout.app')
@section('title', 'Business');

@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-portfolio icon-gradient bg-mean-fruit"></i>
                </div>
                <div>Business
                    <div class="page-title-subheading">Edit Business.</div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card mt-3">
                    <div class="card-header">
                        Edit Business Form {{old('category')}}
                    </div>
                    <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif 
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif   
                    <form action="/dashboard/business/{{$business->id}}" class="was-validated" method="POST" enctype="multipart/form-data">
                        @csrf
                        {{ method_field('PUT') }}
                        <input type="hidden" name="id" value="{{$business->id}}">
                        <div class="row form-group">
                            <div class="col-md-4">
                                <label for="title">Title</label>
                                <input class="form-control" type="text" name="title" required value="{{$business->title}}">
                            </div>
                            <div class="col-md-4">
                                <label for="category">Category</label>
                            
                                <select class="form-control" name="category" id="category" value="{{old('category')}}">
                                    @foreach ($categories as $category)
                                    <option value="{{$category->id}}" {{ old('category', $business->category_id) == $category->id ? 'selected' : '' }}>{{$category->categories_name}}</option>
                                    @endforeach
                                </select>
                                
                            </div>
                            <div class="col-md-4">
                                <label for="subcategory">SubCategory</label>
                                <select name="subcategory" class="form-control" id="subcategory" value="{{old('subcategory')}}" data-selected-subcategory = "{{ old('subcategory') }}" >

                                </select>
                            </div>
                            
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label for="description">Description</label>
                                <textarea class="form-control" name="description" id="description" rows="8" style="height:200px" required>{{$business->description}}</textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="image">Image</label>
                                <input type="file" name="image" id="image" class="dropify">
                            </div>
                        </div>
                        <div class="text-center col-md-12">
                            <button type="submit" class="btn btn-success" name="action" value="update" class="register">save</button>
                            <button type="submit" class="btn btn-danger" name="action" value="cancel" class="register">Cancel</button>
                        </div>

                    </form>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>
@endsection

@push('js')
    <script>
        $('.dropify').dropify();
    </script>
    <script>
        $(window).on('load',function (){
                var category = $('#category').val();
                var business = '{{$business->id}}';
                
                //console.log(b);
                if(category){
                    $.ajax({
                        url: '/dashboard/fetchOldsubCategory/'+ business,
                        type: "GET",
                        data : {"_token":"{{ csrf_token() }}"},
                        dataType: "JSON",
                        success: function(data){
                            if(data){
                                console.log(data)
                                $('#subcategory').empty();
                                $('#subcategory').focus;
                                // $('#subcategory').append('<option value="">-- Select City --</option>');
                                $('select[name="subcategory"]').append('<option value="'+ data.id +'" >'  +data.subcategory_name+ '</option>');
                                // var b= data.id;
                                // console.log(b);
                                // $.each(data,function(key,value){
                                   
                                //    $('select[name="subcategory"]').append('<option value="'+ value.id +'" >'  +value.subcategory_name+ '</option>');
                               
                               

                                // });
                            }else{
                                $('#subcategory').empty();
                            }
                        }
                    })
                }
            })
       
        $(document).ready(function(){
           
           

            $('#category').on('change',function(){
                var categoryId = $(this).val()
                if(categoryId){
                    $.ajax({
                        url : '/dashboard/fetchsubcategory/'+ categoryId,
                        type: "GET",
                        data : {"_token":"{{ csrf_token() }}"},
                        dataType: "JSON",
                        success:function(data){
                            if(data){
                                console.log(data);
                                $('#subcategory').empty();
                                $('#subcategory').focus;
                                $('#subcategory').append('<option value="">-- Select City --</option>');
                                $.each(data,function(key,value){
                                   
                                        $('select[name="subcategory"]').append('<option value="'+ value.id +'" >'  +value.subcategory_name+ '</option>');
                                    
                                    

                                });
                               
                            }else{
                                $('#subcategory').empty();
       
                            }
                        }

                    });
                }else{
                    $('#subcategory').empty();
                }
            }); 
        });
    </script>
    
@endpush